# ContractFundamentals.sol

View Source: [contracts/ContractFundamentals.sol](../contracts/ContractFundamentals.sol)

**↘ Derived Contracts: [accountHandling](accountHandling.md), [Finance](Finance.md)**

**ContractFundamentals**

## Functions

- [endContract()](#endcontract)

### endContract

```js
function endContract() public nonpayable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

## Contracts

* [accountHandling](accountHandling.md)
* [ContractFundamentals](ContractFundamentals.md)
* [Finance](Finance.md)
* [homomorphicVoting](homomorphicVoting.md)
* [Migrations](Migrations.md)
* [StringManipulation](StringManipulation.md)
* [Structures](Structures.md)
* [votingExtend](votingExtend.md)
