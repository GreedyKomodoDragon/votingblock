# votingExtend.sol

View Source: [contracts/votingExtend.sol](../contracts/votingExtend.sol)

**↗ Extends: [homomorphicVoting](homomorphicVoting.md)**

**votingExtend**

## Functions

- [(address _accounts)](#)
- [getOwnVotes()](#getownvotes)
- [donateToElection(uint256 _id)](#donatetoelection)
- [getRefund()](#getrefund)

### 

```js
function (address _accounts) public nonpayable homomorphicVoting 
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _accounts | address |  | 

### getOwnVotes

```js
function getOwnVotes() public view
returns(struct Structures.VoteTableData[], uint256)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### donateToElection

```js
function donateToElection(uint256 _id) external payable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _id | uint256 |  | 

### getRefund

```js
function getRefund() external payable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

## Contracts

* [accountHandling](accountHandling.md)
* [ContractFundamentals](ContractFundamentals.md)
* [Finance](Finance.md)
* [homomorphicVoting](homomorphicVoting.md)
* [Migrations](Migrations.md)
* [StringManipulation](StringManipulation.md)
* [Structures](Structures.md)
* [votingExtend](votingExtend.md)
