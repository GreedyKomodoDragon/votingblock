# Migrations.sol

View Source: [contracts/Migrations.sol](../contracts/Migrations.sol)

**Migrations**

## Contract Members
**Constants & Variables**

```js
address public owner;
uint256 public last_completed_migration;

```

## Modifiers

- [restricted](#restricted)

### restricted

```js
modifier restricted() internal
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

## Functions

- [()](#)
- [setCompleted(uint256 completed)](#setcompleted)

### 

```js
function () public nonpayable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### setCompleted

```js
function setCompleted(uint256 completed) public nonpayable restricted 
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| completed | uint256 |  | 

## Contracts

* [accountHandling](accountHandling.md)
* [ContractFundamentals](ContractFundamentals.md)
* [Finance](Finance.md)
* [homomorphicVoting](homomorphicVoting.md)
* [Migrations](Migrations.md)
* [StringManipulation](StringManipulation.md)
* [Structures](Structures.md)
* [votingExtend](votingExtend.md)
