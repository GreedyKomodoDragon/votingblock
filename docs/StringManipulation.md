# StringManipulation.sol

View Source: [contracts/StringManipulation.sol](../contracts/StringManipulation.sol)

**StringManipulation**

## Functions

- [append(string a, string b)](#append)
- [containsString(string what, string where)](#containsstring)

### append

```js
function append(string a, string b) internal pure
returns(string)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| a | string |  | 
| b | string |  | 

### containsString

```js
function containsString(string what, string where) internal pure
returns(bool)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| what | string |  | 
| where | string |  | 

## Contracts

* [accountHandling](accountHandling.md)
* [ContractFundamentals](ContractFundamentals.md)
* [Finance](Finance.md)
* [homomorphicVoting](homomorphicVoting.md)
* [Migrations](Migrations.md)
* [StringManipulation](StringManipulation.md)
* [Structures](Structures.md)
* [votingExtend](votingExtend.md)
