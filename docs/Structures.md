# Structures.sol

View Source: [contracts/Structures.sol](../contracts/Structures.sol)

**Structures**

## Structs
### Election

```js
struct Election {
 uint256 id,
 uint256 endDate,
 bytes32 name,
 address[] whitelist,
 address owner,
 uint256 balance,
 uint256 etherReward,
 mapping(address => uint256) donors,
 string[] candidateName,
 string[] candidateParty,
 uint8[] candidateRed,
 uint8[] candidateGreen,
 uint8[] candidateBlue,
 uint256 startDate,
 uint8 electionType,
 string description
}
```

### ElectionOutline

```js
struct ElectionOutline {
 bytes32 name,
 address owner,
 uint256 numberOfVotes,
 uint256 id,
 uint256 endDate
}
```

### ElectionTableData

```js
struct ElectionTableData {
 bytes32 name,
 uint256 startDate,
 uint256 endDate,
 uint256 numberOfVotes,
 uint256 id
}
```

### candidate

```js
struct candidate {
 uint256 id,
 string name,
 string party,
 uint8 red,
 uint8 green,
 uint8 blue
}
```

### User

```js
struct User {
 bytes32 password,
 uint256 refundAllocated,
 uint8 questionNumber,
 bytes32 securityAnswer
}
```

### Vote

```js
struct Vote {
 uint256 idElection,
 string[] vote,
 address owner,
 uint256 time
}
```

### VoteTableData

```js
struct VoteTableData {
 bytes32 name,
 uint256 time,
 bytes32 hexValue,
 bool change,
 uint256 electionID
}
```

## Contract Members
**Constants & Variables**

```js
address internal constant OWNER;

```

## Functions

- [owner()](#owner)

### owner

```js
function owner() public pure
returns(address)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

## Contracts

* [accountHandling](accountHandling.md)
* [ContractFundamentals](ContractFundamentals.md)
* [Finance](Finance.md)
* [homomorphicVoting](homomorphicVoting.md)
* [Migrations](Migrations.md)
* [StringManipulation](StringManipulation.md)
* [Structures](Structures.md)
* [votingExtend](votingExtend.md)
