# Use Case Descriptions

## Sign Up

| Use case Field  | Description  |
|---|---|
|  Name | Sign Up |
| Actor(s)  | Voter  |
| Pre-conditions  | - Voter has an ethereum wallet <br/> - Voter has MetaMask installed <br/> - Voter has an email address |
| Termination Outcome: Successful | - Voter's Address is recorded <br/> - Voter is given an account <br/> - Redirected to Account section |
| Termination Outcome: Unsuccessful | - No redirection happens <br/> - A error message will be sent to the user |
| Conditions Affecting Outcome |- Whether user signs the transaction or not <br/> - Validity of Email Address <br/> - Public Address must be valid and not already signed up <br/> - Password must conform to standard set <br/> - Passwords match <br/> - Voter must have enough gas |
| Description | Before a user is able to use all of the features of the website they must first sign in, this requires them to provide an ethereum public address and a email address. The user will be presented with a form to provide these details into. The voter will then fill in these details and then submit the form. Once submitted and valid they will be presented the Metamask transaction to sign. Dependent on whether they sign or not will decide if the account is made or not.  |
| Associations | - Sign Transaction |
| Inputs of User | - Email Address <br/> - Ethereum Address <br/> - Two Passwords (same input for success) <br/> - Sign Transaction  |


## Create Election

| Use case Field  | Description  |
|---|---|
|  Name |  Create Election |
| Actor(s)  | Admin  |
| Pre-conditions  | - Admin is sign in <br/> - Admin has set duration <br/> - Admin has added at least two Candidates <br/> - Admin has provided name |
| Termination Outcome: Successful | - Election has been created <br/> - Election has been added to their account's creation election <br/> - Admin redirect to their account settings |
| Termination Outcome: Unsuccessful | - An error message is present to the admin within the form |
| Conditions Affecting Outcome | - All field inputs <br/> - Admin has enough Ether to make designed election <br/> - Transaction signature  |
| Description | In order to create a election the admin must go through the stages laid out by the website. They must give a name and an end date, then add candidates to the election (with name, party and party colour). The admin must then decide if they are going to give a deposit or reward (or both) in the election and if so how much of each, as well as who can vote. The last stage is to then submit the election, a signature is required to move the election the contract. |
| Associations | - Add Whitelist <br/> - Deposit Ether <br/> - Set Duration <br/> - Add Candidates <br/> - Edit Candidates <br/> - Remove Candidates |
| Inputs of User | - Signature for transaction |
