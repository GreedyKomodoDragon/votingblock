# Encryption and Hashing

## Encryption
Encryption is used to keep the votes safe from others seeing how others vote on the election. This is achieve via using the Paillier cryptosystem. This allows for the encryption of the data into a form that cannot be interpreted but can be used in simple operations such as addition. How this works is explained below:

#### Key generation
1.  Generate two large prime numbers; such that  $`gcd(pq, (p-1)(q-1)) = 1`$
2.  Compute the $` \lambda = lowestCommonMultiple(p-1, q-1)`$
3.  Select a random integer that aligns to $`g \in {\mathbb{Z}_{n^2}^{*}}`$
4.  Ensure $`n`$ divides the order of $`g`$ by checking the existence of the following:
```math
\mu = (L(g^\lambda \thinspace mod \thinspace n^2))^{-1}
```
Such that L:
```math
L(x) = \frac {x - 1}{n}
```

*  Public Key: ($`n`$, $`g`$)
*  Private Key: ($`\lambda`$, $`\mu`$)

#### Encryption
Compute ciphertext using: $`c =  g^m  \cdot  r^n \thinspace mod \thinspace n^2 `$

Such that
1.  $`m`$ is the message between $`0  \leq m < n`$
2.  Select a random r where  $`0 < r < n`$ and $`r \in {\mathbb{Z}_{n^2}^{*}}`$

#### Decryption
Compute the plaintext message using: $`\mu = L(c^\lambda \thinspace mod \thinspace n^2)`$

#### Homomorphic addition of plaintexts
Addition of two ciphertexts will decrypt to the sum of the plaintext values:
$`D(E( m_1 ,  r_1 ) \cdot E( m_2 ,  r_2 ) \thinspace mod \thinspace n^2) = m_1 + m_2 \thinspace mod \thinspace n`$


## Hashing

Within the application hashing is used in two places; both within the Node Js backend. The first use of hashing the vote before it has been stored in the smart contract for verifaction use later on. The second hashing is used when the password is stored.

### Hashing the Vote
The hashing algorithm used is: [web3.utils.soliditysha3](https://web3js.readthedocs.io/en/v1.2.0/web3-utils.html#soliditysha3)