# accountHandling.sol

View Source: [contracts/accountHandling.sol](../contracts/accountHandling.sol)

**↗ Extends: [ContractFundamentals](ContractFundamentals.md)**

**accountHandling**

## Contract Members
**Constants & Variables**

```js
//public members
mapping(address => struct Structures.User) public userDetails;
mapping(address => uint256[]) public whitelisted;
mapping(address => bool) public addressWhiteList;

//internal members
string[] internal SECURITY_QUESTIONS;

```

## Functions

- [login(bytes32 hashedPassword)](#login)
- [changePassword(bytes32 hashedAnswer, bytes32 hashPassword)](#changepassword)
- [changePasswordWithOldPassword(bytes32 _hashedPasswordOld, bytes32 _hashedPasswordNew)](#changepasswordwitholdpassword)
- [updateSecurityQuestion(uint8 _questionNumber, bytes32 _hashedAnswer)](#updatesecurityquestion)
- [addNewAddress(bytes32 _hashedPassword, uint8 _questionNumber, bytes32 _hashedAnswer)](#addnewaddress)
- [isRegistered(address sender)](#isregistered)
- [getSecurityQuestion()](#getsecurityquestion)
- [addRefund(address user, uint256 amount)](#addrefund)
- [checkRefundAmount()](#checkrefundamount)
- [getwhitelist()](#getwhitelist)
- [addUserToWhitelist(address _user, uint256 _electionID)](#addusertowhitelist)
- [clearRefund(address clear)](#clearrefund)

### login

```js
function login(bytes32 hashedPassword) public view
returns(bool)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| hashedPassword | bytes32 |  | 

### changePassword

```js
function changePassword(bytes32 hashedAnswer, bytes32 hashPassword) external nonpayable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| hashedAnswer | bytes32 |  | 
| hashPassword | bytes32 |  | 

### changePasswordWithOldPassword

```js
function changePasswordWithOldPassword(bytes32 _hashedPasswordOld, bytes32 _hashedPasswordNew) external nonpayable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _hashedPasswordOld | bytes32 |  | 
| _hashedPasswordNew | bytes32 |  | 

### updateSecurityQuestion

```js
function updateSecurityQuestion(uint8 _questionNumber, bytes32 _hashedAnswer) external nonpayable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _questionNumber | uint8 |  | 
| _hashedAnswer | bytes32 |  | 

### addNewAddress

```js
function addNewAddress(bytes32 _hashedPassword, uint8 _questionNumber, bytes32 _hashedAnswer) external nonpayable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _hashedPassword | bytes32 |  | 
| _questionNumber | uint8 |  | 
| _hashedAnswer | bytes32 |  | 

### isRegistered

```js
function isRegistered(address sender) public view
returns(bool)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| sender | address |  | 

### getSecurityQuestion

```js
function getSecurityQuestion() public view
returns(string)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### addRefund

```js
function addRefund(address user, uint256 amount) external nonpayable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| user | address |  | 
| amount | uint256 |  | 

### checkRefundAmount

```js
function checkRefundAmount() external view
returns(uint256)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### getwhitelist

```js
function getwhitelist() external view
returns(uint256[])
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### addUserToWhitelist

```js
function addUserToWhitelist(address _user, uint256 _electionID) external nonpayable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _user | address |  | 
| _electionID | uint256 |  | 

### clearRefund

```js
function clearRefund(address clear) external nonpayable
returns(uint256)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| clear | address |  | 

## Contracts

* [accountHandling](accountHandling.md)
* [ContractFundamentals](ContractFundamentals.md)
* [Finance](Finance.md)
* [homomorphicVoting](homomorphicVoting.md)
* [Migrations](Migrations.md)
* [StringManipulation](StringManipulation.md)
* [Structures](Structures.md)
* [votingExtend](votingExtend.md)
