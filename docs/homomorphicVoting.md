# homomorphicVoting.sol

View Source: [contracts/homomorphicVoting.sol](../contracts/homomorphicVoting.sol)

**↗ Extends: [Finance](Finance.md)**
**↘ Derived Contracts: [votingExtend](votingExtend.md)**

**homomorphicVoting**

## Contract Members
**Constants & Variables**

```js
//internal members
mapping(uint256 => uint256[]) internal results;
mapping(uint256 => bool) internal resultsSubmitted;

//public members
struct Structures.Election[] public elections;
mapping(uint256 => struct Structures.Vote[]) public votes;

```

## Functions

- [(address _accounts)](#)
- [getOwnersElectionIDs()](#getownerselectionids)
- [getElectionBasedOnName(string contains)](#getelectionbasedonname)
- [createElection(struct Structures.candidate[] _candidates, bytes32 _nameOfElection, uint8 _electionType, uint256 _days, address[] whitelist, uint256 _reward, string _description)](#createelection)
- [getElection(uint256 _electionID)](#getelection)
- [getElectionDescription(uint256 _electionID)](#getelectiondescription)
- [getNumberOfPolls()](#getnumberofpolls)
- [getNumberOfVotes(uint256 _electionID)](#getnumberofvotes)
- [addVote(uint256 _electionID, string[] _vote)](#addvote)
- [getVotes(uint256 _electionID)](#getvotes)
- [verifyVote(uint256 _electionID, bytes32 _hex)](#verifyvote)
- [isWhitelistedVoter(address _votee, uint256 _poll)](#iswhitelistedvoter)
- [submitResults(uint256 _electionID, uint256[] _results)](#submitresults)
- [hasResults(uint256 _electionID)](#hasresults)
- [getResults(uint256 _electionID)](#getresults)

### 

```js
function (address _accounts) public nonpayable Finance 
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _accounts | address |  | 

### getOwnersElectionIDs

```js
function getOwnersElectionIDs() public view
returns(uint256[])
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### getElectionBasedOnName

```js
function getElectionBasedOnName(string contains) public view
returns(struct Structures.ElectionOutline[], uint256)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| contains | string |  | 

### createElection

```js
function createElection(struct Structures.candidate[] _candidates, bytes32 _nameOfElection, uint8 _electionType, uint256 _days, address[] whitelist, uint256 _reward, string _description) public payable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _candidates | struct Structures.candidate[] |  | 
| _nameOfElection | bytes32 |  | 
| _electionType | uint8 |  | 
| _days | uint256 |  | 
| whitelist | address[] |  | 
| _reward | uint256 |  | 
| _description | string |  | 

### getElection

```js
function getElection(uint256 _electionID) external view
returns(uint256, bytes32, uint256, address, uint256, uint256, string[], string[], uint8[], uint8[], uint8[], uint8)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _electionID | uint256 |  | 

### getElectionDescription

```js
function getElectionDescription(uint256 _electionID) external view
returns(string)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _electionID | uint256 |  | 

### getNumberOfPolls

```js
function getNumberOfPolls() public view
returns(uint256)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### getNumberOfVotes

```js
function getNumberOfVotes(uint256 _electionID) public view
returns(uint256)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _electionID | uint256 |  | 

### addVote

```js
function addVote(uint256 _electionID, string[] _vote) public payable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _electionID | uint256 |  | 
| _vote | string[] |  | 

### getVotes

```js
function getVotes(uint256 _electionID) public view
returns(struct Structures.Vote[])
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _electionID | uint256 |  | 

### verifyVote

```js
function verifyVote(uint256 _electionID, bytes32 _hex) external view
returns(bool)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _electionID | uint256 |  | 
| _hex | bytes32 |  | 

### isWhitelistedVoter

```js
function isWhitelistedVoter(address _votee, uint256 _poll) internal view
returns(bool)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _votee | address |  | 
| _poll | uint256 |  | 

### submitResults

```js
function submitResults(uint256 _electionID, uint256[] _results) external nonpayable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _electionID | uint256 |  | 
| _results | uint256[] |  | 

### hasResults

```js
function hasResults(uint256 _electionID) public view
returns(bool)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _electionID | uint256 |  | 

### getResults

```js
function getResults(uint256 _electionID) external view
returns(uint256[])
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _electionID | uint256 |  | 

## Contracts

* [accountHandling](accountHandling.md)
* [ContractFundamentals](ContractFundamentals.md)
* [Finance](Finance.md)
* [homomorphicVoting](homomorphicVoting.md)
* [Migrations](Migrations.md)
* [StringManipulation](StringManipulation.md)
* [Structures](Structures.md)
* [votingExtend](votingExtend.md)
