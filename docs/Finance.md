# Finance.sol

View Source: [contracts/Finance.sol](../contracts/Finance.sol)

**↗ Extends: [ContractFundamentals](ContractFundamentals.md)**
**↘ Derived Contracts: [homomorphicVoting](homomorphicVoting.md)**

**Finance**

## Contract Members
**Constants & Variables**

```js
mapping(uint256 => address[]) internal donorKeys;
contract accountHandling internal accounts;

```

## Functions

- [(address _accounts)](#)
- [getEtherNeed(struct Structures.Election election, uint256 _etherNeeded)](#getetherneed)
- [donorsHaveEnough(struct Structures.Election election, uint256 weiAmount)](#donorshaveenough)
- [refundUsers(struct Structures.Election election, address ownerFound)](#refundusers)

### 

```js
function (address _accounts) public nonpayable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _accounts | address |  | 

### getEtherNeed

```js
function getEtherNeed(struct Structures.Election election, uint256 _etherNeeded) internal nonpayable
returns(uint256)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| election | struct Structures.Election |  | 
| _etherNeeded | uint256 |  | 

### donorsHaveEnough

```js
function donorsHaveEnough(struct Structures.Election election, uint256 weiAmount) internal view
returns(bool)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| election | struct Structures.Election |  | 
| weiAmount | uint256 |  | 

### refundUsers

```js
function refundUsers(struct Structures.Election election, address ownerFound) internal nonpayable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| election | struct Structures.Election |  | 
| ownerFound | address |  | 

## Contracts

* [accountHandling](accountHandling.md)
* [ContractFundamentals](ContractFundamentals.md)
* [Finance](Finance.md)
* [homomorphicVoting](homomorphicVoting.md)
* [Migrations](Migrations.md)
* [StringManipulation](StringManipulation.md)
* [Structures](Structures.md)
* [votingExtend](votingExtend.md)
