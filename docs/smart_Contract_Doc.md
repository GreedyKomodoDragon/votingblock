# Homomorphic Voting Contract

Methods that can be called by an externally on the contract are documentated below

### External Methods

isAddressInWhiteList 
- Method Specialisation: view
- Purpose:
    - Will return true if address has been signed up
    - Will return false if address has not been signed up
- Params
    - Address Check
- Return type: bool

addNewAddress 
- Purpose:
    - Will add msg.sender value to the whitelist
    - Emits UserAdded Event
-Error
    - Will reverse the changes if address has already been signed up 
- Params
    - N/A

createPoll 
- Purpose:
    - Creates a new election
    - Emits ElectionCreated Event
-Error
    - Will reverse the changes if msg.sender has not signed up 
    - Will reverse the reward is negative
- Params
    - uint _numberOfCandidates
    - uint _days
    - address[] memory whitelist
    - uint256 _reward

addVote 
- Purpose:
    - Adds vote to an election
    - Emits Voted Event
-Error
    - Will reverse the changes if msg.sender has not signed up 
    - Will reverse if, you vote on a election that has already passed
    - Will reverse if, you are not within the whitelist if one is active
- Params
    - uint _electionID
    - string[] memory _vote

getVotes
- Purpose:
    - returns all the vote hashes for an election
-Error
    - Will reverse the changes if msg.sender has not signed up
- Params
    - uint _electionID
- Return type: uint

getNumberOfPolls
- Purpose:
    - returns the number of elections
-Error
    - Will reverse the changes if msg.sender has not signed up
- Params
    - N/A
- Return type: uint


getNumberOfVotes
- Purpose:
    - Will return the number of votes in a election
-Error
    - Will reverse the changes if msg.sender has not signed up
- Params
    - uint _electionID


verifyVote
- Purpose:
    - used to verify that a vote has been editted or not in the database
-Error
    - Will reverse the changes if msg.sender has not signed up
- Params
    - uint _electionID
    - bytes32 _hex


isWhitelistedVoter
- Purpose:
    - check if the user is whitelisted in an election
- Params
    - address _votee
    - uint _poll
- Return type: bool

getwhitelist
- Purpose:
    - returns all of the elections the user is whitelisted on
-Error
    - Will reverse the changes if msg.sender has not signed up

- return type: uint[] memory


