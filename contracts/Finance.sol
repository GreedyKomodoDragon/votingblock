pragma solidity ^0.6.4;

import {Structures} from "./Structures.sol";
import {accountHandling} from "./accountHandling.sol";
import {ContractFundamentals} from "./ContractFundamentals.sol";


contract Finance is ContractFundamentals {

    mapping(uint => address[]) donorKeys;

    accountHandling accounts;

    constructor (address _accounts) public {
        accounts = accountHandling(_accounts);
    }
    
    function getEtherNeed(Structures.Election storage election, uint _etherNeeded) internal returns(uint) {
        //loop through all the donors and keep track 
        uint subToZero = _etherNeeded;

        for (uint j = 0; j<donorKeys[election.id].length; j++) {

            //check if the donor has ether left to use
            if (election.donors[donorKeys[election.id][j]] != 0) {

                //check if donor has enough or partial amount
                if (election.donors[donorKeys[election.id][j]]<subToZero) {
                    subToZero = subToZero - election.donors[donorKeys[election.id][j]];
                    election.donors[donorKeys[election.id][j]] = 0;
                }else {
                    election.donors[donorKeys[election.id][j]] = election.donors[donorKeys[election.id][j]] - subToZero;
                    subToZero = 0;
                }

            }
            
        }

        return _etherNeeded - subToZero;
    }

    function donorsHaveEnough(Structures.Election storage election, uint weiAmount) internal view returns(bool){
        uint sum = 0;

        for (uint i = 0; i < donorKeys[election.id].length; i++) {
            sum = sum + election.donors[donorKeys[election.id][i]];
            if (sum > weiAmount) {
                return true;
            }
        }

        return false;
    }

    function refundUsers (Structures.Election storage election, address ownerFound) internal {
        //return the balance of the initial to the user
        if (election.balance != 0) {
            accounts.addRefund(ownerFound, election.balance);
            election.balance = 0;
        }

        //refund the users
        for (uint i = 0; i<donorKeys[election.id].length; i++) {
            //address of donor
            address donor = donorKeys[election.id][i];
            accounts.addRefund(ownerFound, election.donors[donor]);
            election.donors[donor] = 0;
        }
    }

}