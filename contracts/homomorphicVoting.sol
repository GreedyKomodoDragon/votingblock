pragma experimental ABIEncoderV2;
pragma solidity ^0.6.4;

import {Finance} from "./Finance.sol";
import {StringManipulation} from "./StringManipulation.sol";
import {Structures} from "./Structures.sol";
import {accountHandling} from "./accountHandling.sol";

contract homomorphicVoting is Finance {
    
    mapping(uint => uint[]) results;
    mapping(uint => bool) resultsSubmitted;

    Structures.Election[] public elections;

    mapping (uint => Structures.Vote[]) public votes;

    constructor (address _accounts) Finance(_accounts) public {}

    function getOwnersElectionIDs() public view returns(uint[] memory){
        require(accounts.isRegistered(msg.sender)); //check user is registered

        uint numberOfElections = 0;

        //find out the number of elections the owner has
        //required as no dynamic array
        for(int i = int(elections.length-1); i>-1; i--){
            if(msg.sender == elections[uint(i)].owner){
                numberOfElections += 1;
            }
        }

        uint[] memory electionIDs = new uint[](numberOfElections);
        uint count = 0;

        for(int i = int(elections.length-1); i>-1; i--){
            //name contains the string
            if(msg.sender == elections[uint(i)].owner){
                electionIDs[count] = uint(i);
                count = count + 1;
            }
        }

        return electionIDs;

    }
    
    //uint positionStarter
    function getElectionBasedOnName(string memory contains) public view returns(Structures.ElectionOutline[] memory, uint){
        require(accounts.isRegistered(msg.sender)); //check user is registered
         Structures.ElectionOutline[] memory electionsFound = new  Structures.ElectionOutline[](10);

        uint8 count = 0;

        for(int i = int(elections.length-1); i>-1 && count != 10; i--){

            bytes memory bytesArray = new bytes(32);
            for (uint8 k; k < 32; k++) {
                bytesArray[k] = elections[uint(i)].name[k];
            }

            if(StringManipulation.containsString(contains, string(bytesArray))){
                electionsFound[count].name = elections[uint(i)].name;
                electionsFound[count].numberOfVotes = votes[uint(i)].length;
                electionsFound[count].owner = elections[uint(i)].owner;
                electionsFound[count].id = uint(i);
                electionsFound[count].endDate = elections[uint(i)].endDate;
                count = count + 1;
            }
        }

        return (electionsFound, count);
    }

    function createElection(Structures.candidate[] memory _candidates, bytes32 _nameOfElection , uint8 _electionType, uint _days, address[] memory whitelist, uint256 _reward, string memory _description) public payable {
        require(accounts.isRegistered(msg.sender)); //check user is registered
        require(_reward >= 0); //make sure reward is not negative

        uint endDate = block.timestamp + _days * 1 days; //creates the end date using internal timing

        //extract candidate data
        string[] memory candidateName_ = new string[](_candidates.length);
        string[] memory candidateParty_ = new string[](_candidates.length);
        uint8[] memory candidateRed_ = new uint8[](_candidates.length);
        uint8[] memory candidateGreen_ = new uint8[](_candidates.length);
        uint8[] memory candidateBlue_ = new uint8[](_candidates.length);

        for(uint i =  0; i < _candidates.length; i++){
            candidateName_[i] = _candidates[i].name;
            candidateParty_[i] = _candidates[i].party;
            candidateRed_[i] = _candidates[i].red;
            candidateGreen_[i] = _candidates[i].green;
            candidateBlue_[i] = _candidates[i].blue;
        }
        
        //creates new election
        Structures.Election memory newElection =  Structures.Election({
            id: elections.length,
            endDate: endDate,
            name:_nameOfElection,
            whitelist: whitelist, 
            owner: msg.sender, 
            balance: msg.value,
            etherReward: _reward,
            candidateName: candidateName_,
            candidateParty: candidateParty_,
            candidateRed: candidateRed_,
            candidateGreen: candidateGreen_,
            candidateBlue: candidateBlue_,
            startDate: block.timestamp,
            electionType: _electionType,
            description: _description
            }); 
        

        for (uint i = 0; i < whitelist.length; i++) {
            accounts.addUserToWhitelist(whitelist[i], elections.length);
        }

        elections.push(newElection); 
    }

    function getElection(uint _electionID) external view returns(uint, bytes32, uint, address, uint, uint, string[] memory, string[] memory, uint8[] memory, uint8[] memory, uint8[] memory, uint8){
       require(accounts.isRegistered(msg.sender)); //check user is registered
        require(_electionID < elections.length); //make sure it is a valid election

        //require due to: CompilerError: Stack too deep, try removing local variables.
         Structures.Election memory selected = elections[_electionID];

        //required to return like this due to: TypeError: Internal or recursive type is not allowed for public or external functions.
        return (selected.endDate, 
                selected.name, 
                selected.startDate,
                selected.owner,
                selected.balance,
                selected.etherReward,
                selected.candidateName,
                selected.candidateParty,
                selected.candidateRed,
                selected.candidateGreen,
                selected.candidateBlue,
                selected.electionType
                 );
    }

    function getElectionDescription(uint _electionID) external view returns(string memory){
        require(accounts.isRegistered(msg.sender)); //check user is registered
        require(_electionID < elections.length); //make sure it is a valid election

         //require due to: CompilerError: Stack too deep, try removing local variables.
         Structures.Election memory selected = elections[_electionID];

         return selected.description;

    }

    function getNumberOfPolls() public view returns(uint){
        require(accounts.isRegistered(msg.sender) || msg.sender == Structures.owner()); //check user is registered 
        return elections.length;
    }

    function getNumberOfVotes(uint _electionID) public view returns(uint){
        require(accounts.isRegistered(msg.sender)); //check user is registered
        return votes[_electionID].length;
    }

    function addVote(uint _electionID, string[] memory _vote) public payable {
        require(accounts.isRegistered(msg.sender)); //check user is registered
        require(isWhitelistedVoter(msg.sender, _electionID)); //checking that user can vote on this election

        uint256 startGas = gasleft(); //keep record of gas at start

        //looking for if the user has voted already
        bool hasFoundVote = false;
        uint k = 0;
        for(k; k<votes[_electionID].length; k++){
            if(votes[_electionID][k].owner == msg.sender){
                hasFoundVote = true;
                break;
            }
        }

        //create the vote
         Structures.Vote memory newVote =  Structures.Vote(_electionID, _vote, msg.sender, block.timestamp);


        if(hasFoundVote){ 
            //if they have voted before replace it
            votes[_electionID][k] = newVote;

            //ends here as you do not get a reward for re-voting 
            //stops users abusing the system

        }
        else{             
            //if they have not add it to the end
            votes[_electionID].push(newVote);

            //workout gas used, will not be 100% due to changes to ether in contract
            uint256 gasUsed = startGas - gasleft(); 


            //amount you get back
            uint etherNeeded = gasUsed + elections[_electionID].etherReward;


            //check if gas left, if none left user will have to pay
            if(elections[_electionID].balance >= etherNeeded){
                //remove gas from balance and reward
                elections[_electionID].balance -= etherNeeded; 

                //send refund
                msg.sender.transfer(etherNeeded);
            }else if(donorsHaveEnough(elections[_electionID], etherNeeded)){
                //for each donor remove ether until removed enough
                msg.sender.transfer(getEtherNeed(elections[_electionID],etherNeeded));
            }
        }
    }

    //restrict this to the owner only for getting the results
    function getVotes(uint _electionID) public view returns(Structures.Vote[] memory)  {
        require(msg.sender == Structures.owner() || hasResults(_electionID), "Not the owner or has not been submitted"); //check user is registered
        //require(elections[_electionID].endDate < block.timestamp, "Time not up yet"); //You can only get the votes once the election has ended
        return votes[_electionID];    
    }   
    
    function verifyVote(uint _electionID, bytes32 _hex) external view returns(bool){
        require(accounts.isRegistered(msg.sender)); //check user is registered

        //searching for the users hex
        for(uint i = 0; i<votes[_electionID].length; i++){
            if(votes[_electionID][i].owner == msg.sender){
                
                string memory preHex = "";
                for(uint j = 0; j < votes[_electionID][i].vote.length; j++){
                        preHex = StringManipulation.append(preHex, votes[_electionID][i].vote[j]);
                        
                }

                return keccak256(abi.encode(preHex)) == _hex;
                 
            }
        }

        //Will return false if: hex does not match || user has not voted
        return false;
    } 

    function isWhitelistedVoter(address _votee, uint _poll) internal view returns(bool){
        
        if(elections[_poll].whitelist.length == 0){ //if it is empty is means the user did not implement it
            return true;
        }else{
            //search through all valid addresses
            for(uint i = 0; i< elections[_poll].whitelist.length; i++){
                if(elections[_poll].whitelist[i]==_votee){
                    return true;
                }
            }

            //returns false if you cannot find the address in the whitelist
            return false;
        }
    }

    //submit results
    function submitResults(uint _electionID, uint[] calldata _results) external {
        require(msg.sender == Structures.owner()); //Only Owner of the contract can submit results
        require(_electionID < elections.length); //check electionID is correct
        //require(elections[_electionID].endDate < block.timestamp, "Has not finished"); //election must finish
        require(_results.length == elections[_electionID].candidateName.length); //check number of candidates is equal
        require(!resultsSubmitted[_electionID]); //has not had results already submitted

        //submit results
        results[_electionID] = _results;
        resultsSubmitted[_electionID] = true;

        address ownerFound = elections[_electionID].owner;

        // Removing all left over ether to users to claim
        refundUsers(elections[_electionID], ownerFound);

    }

    function hasResults(uint _electionID) public view returns (bool){
        return resultsSubmitted[_electionID];
    }

    //get the results
    function getResults(uint _electionID) external view returns (uint[] memory){
        require(accounts.isRegistered(msg.sender) || msg.sender == Structures.owner());
        require(resultsSubmitted[_electionID]);
        //require(elections[_electionID].endDate < block.timestamp);
        return(results[_electionID]);
    }

}