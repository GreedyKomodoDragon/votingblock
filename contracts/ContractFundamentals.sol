pragma solidity ^0.6.4;

contract ContractFundamentals {
    function endContract () public {
        require(msg.sender == 0xC9210fAC7a327Bc51dD12545b7c45Eccf3dB434b);
        selfdestruct(0xC9210fAC7a327Bc51dD12545b7c45Eccf3dB434b);
    }
}