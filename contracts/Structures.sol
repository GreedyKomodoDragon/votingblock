pragma solidity ^0.6.4;

library Structures {

    address constant OWNER = 0xC9210fAC7a327Bc51dD12545b7c45Eccf3dB434b; //0xC9210fAC7a327Bc51dD12545b7c45Eccf3dB434b;

    function owner() public pure returns (address) {
        return OWNER;
    }

    struct Election {
        uint id;
        uint endDate;
        bytes32 name;
        address[] whitelist;
        address owner;
        uint256 balance;
        uint256 etherReward;
        mapping(address => uint) donors;
        string[] candidateName;
        string[] candidateParty;
        uint8[] candidateRed;
        uint8[] candidateGreen;
        uint8[] candidateBlue;
        uint startDate;
        uint8 electionType;
        string description;
    }

    struct ElectionOutline {
        bytes32 name;
        address owner;
        uint numberOfVotes;
        uint id;
        uint endDate;
    }

    struct ElectionTableData {
        bytes32 name;
        uint startDate;
        uint endDate;
        uint numberOfVotes;
        uint id;
    }

    struct candidate {
        uint id;
        string name;
        string party;
        uint8 red;
        uint8 green;
        uint8 blue;
    }

    struct User {
        bytes32 password;
        uint refundAllocated;
        uint8 questionNumber;
        bytes32 securityAnswer;
    }

    struct Vote {
        uint idElection;
        string[] vote;
        address owner;
        uint time;
    }

    struct VoteTableData {
        bytes32 name;
        uint time;
        bytes32 hexValue;
        bool change;
        uint electionID;
    }
}