pragma solidity ^0.6.4;

import {Structures} from "./Structures.sol";
import {ContractFundamentals} from "./ContractFundamentals.sol";

contract accountHandling is ContractFundamentals {
    //user-related maps
    mapping(address => Structures.User) public userDetails; //holds details of user

    //holds all of the whitelisted elections for each address, made for easier access
    mapping (address => uint[]) public whitelisted;

    //only addresses that return true have been registered
    mapping(address => bool) public addressWhiteList;

    string[] SECURITY_QUESTIONS = [
        "What was the name of your second pet?",
        "What was the last name of your third-grade teacher?",
        "Who was your childhood hero?"
    ];


    //adds extra layer of authenication
    function login(bytes32 hashedPassword) public view returns(bool){
        require(addressWhiteList[msg.sender]); //check  already registered
        return(userDetails[msg.sender].password == hashedPassword);
    }


    function changePassword(bytes32 hashedAnswer, bytes32 hashPassword) external {
            require(addressWhiteList[msg.sender]); //check  already registered
            require(userDetails[msg.sender].securityAnswer == hashedAnswer);

            userDetails[msg.sender].password = hashPassword;            
        }

    function changePasswordWithOldPassword(bytes32 _hashedPasswordOld, bytes32 _hashedPasswordNew) external {
        require(addressWhiteList[msg.sender]); //check  already registered
        require(login(_hashedPasswordOld)); //old password matches
        require(userDetails[msg.sender].password == _hashedPasswordOld);
        
        userDetails[msg.sender].password = _hashedPasswordNew;        
    }

    function updateSecurityQuestion(uint8 _questionNumber, bytes32 _hashedAnswer) external {
        require(addressWhiteList[msg.sender]); //check  already registered
        userDetails[msg.sender].questionNumber = _questionNumber;
        userDetails[msg.sender].securityAnswer = _hashedAnswer;
    }

    function addNewAddress(bytes32 _hashedPassword, uint8 _questionNumber, bytes32 _hashedAnswer) external {
        require(!addressWhiteList[msg.sender]); //check not already registered

        //whitelisted address for contract access
        addressWhiteList[msg.sender] = true; 

        //create user account
        userDetails[msg.sender] = Structures.User(_hashedPassword, 0, _questionNumber, _hashedAnswer);

        //intialise whitelist
        whitelisted[msg.sender]; 
    }

    function isRegistered(address sender) public view returns (bool){
        return addressWhiteList[sender];
    }

    function getSecurityQuestion() public view returns(string memory) {
        require(addressWhiteList[msg.sender]); //check user is registered
        return SECURITY_QUESTIONS[userDetails[msg.sender].questionNumber];
    }

    function addRefund(address user, uint amount) external {
        userDetails[user].refundAllocated += amount;
    }

    function checkRefundAmount() view external returns(uint) {
       require(addressWhiteList[msg.sender]); //check user is registered 
       return userDetails[msg.sender].refundAllocated;
    }

    function getwhitelist() external view returns(uint[] memory) {
        return whitelisted[msg.sender];
    }

    function addUserToWhitelist(address _user, uint _electionID) external {
        whitelisted[_user].push(_electionID);
    }

    function clearRefund(address clear) external returns(uint) {
        require(addressWhiteList[clear]); //check user is registered 
        uint32 size;
        address addr = msg.sender;
        assembly {
            size := extcodesize(addr)
        }

        //check if it is a contract call
        uint amount = 0;
        if (size > 0) {
            amount = userDetails[clear].refundAllocated;
            userDetails[clear].refundAllocated = 0;
        }       

        return amount;
    }
}