pragma experimental ABIEncoderV2;
pragma solidity ^0.6.4;

import {Structures} from "./Structures.sol";
import {homomorphicVoting} from "./homomorphicVoting.sol";
import {StringManipulation} from "./StringManipulation.sol";

contract votingExtend is homomorphicVoting {

    constructor (address _accounts) homomorphicVoting(_accounts) public {}


    function getOwnVotes() public view returns (Structures.VoteTableData[] memory, uint){
        require(accounts.isRegistered(msg.sender)); //check user is registered

        Structures.VoteTableData[] memory tableVotes = new Structures.VoteTableData[](10);

        uint count = 0;

        for(uint i = 0; i < elections.length; i++){
            for(uint k = 0; k < votes[i].length; k++){
                if(votes[i][k].owner == msg.sender){

                    //set data to found data
                    tableVotes[count].name = elections[i].name;
                    tableVotes[count].electionID = i;

                    //convert hex
                    string memory preHex = "";

                    for(uint j = 0; j < votes[i][k].vote.length; j++){
                        preHex = StringManipulation.append(preHex, votes[i][k].vote[j]);
                    }

                    tableVotes[count].hexValue = keccak256(abi.encode(preHex));


                    tableVotes[count].change = elections[i].endDate > block.timestamp;
                    tableVotes[count].time = votes[i][k].time;

                    count++;
                }      
            }
                 
        }
        return(tableVotes, count);

    }

    function donateToElection(uint _id) external payable{
        require(accounts.isRegistered(msg.sender)); //check user is registered
        require(!hasResults(_id));

        //if you are the owner then add to the balance instead
        if(msg.sender == elections[_id].owner){
            elections[_id].balance += msg.value;
        }else{
            //Add Donor to keylist
            donorKeys[_id].push(msg.sender);

            //check if a user has already donated
            if(elections[_id].donors[msg.sender] != 0){
                elections[_id].donors[msg.sender] = elections[_id].donors[msg.sender] + msg.value;
            }else{
                elections[_id].donors[msg.sender] = msg.value;
            }
        }
       
    }

    function getRefund() external payable {
        require(accounts.isRegistered(msg.sender)); //check user is registered
        uint amount = accounts.clearRefund(msg.sender);
        require(amount > 0);
        msg.sender.transfer(amount);       
    }
}