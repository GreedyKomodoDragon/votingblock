# Voting Block

Voting Block is a project that utilises the properties of the blockchain to allow for a voting application that allows for the verification of votes, where using encryption to hide voting information from other voters/attackers.

### Motivation

Blockchain technology allows for the verification of votes because of the immutability the blockchain provides via the cryptographic technologies such as hashing the blockchain will implement. Ensuring that every election only uses votes that are valid and have been untampered with. The motivation for such a project comes from recent events such as the 2016 US Election where there was a notation that the election may have been tampered with, although not confirmed, an election based on the blockchain would ensure no vote has been tempered with and if it has then it will be apparent to the electoral administers that it has been.

### Implementation and Technologies

The Project is separated into 3 distinct aspects. The backend server, frontend web client and the smart contract on the Ethereum blockchain. Both the backend and frontend are written predominantly in [TypeScript](https://www.typescriptlang.org/) with a small section written in JavaScript. The backend uses NodeJS With the [Express Framework](https://expressjs.com/). The frontend uses the [ReactJS Framework](https://reactjs.org/). The Smart Contract is written using [Solidity](https://github.com/ethereum/solidity) - "Solidity is a statically typed, contract-oriented, high-level language for implementing smart contracts on the Ethereum platform".

## Getting Started
#### Cloning
```
git clone https://cseegit.essex.ac.uk/ce301_2019/ce301_chapman_j.git
```

#### Dependencies
You will need to install NodeJS into order to use this repository. Go to [Mozilla](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/development_environment) and follow the setting up of NodeJS documentation if you have not already installed NodeJS.

Version of NodeJS used: v11.12.0

Ganache and Truffle will need to be set up in order to use the smart contract on a local blockchain. [Download ganache](https://github.com/trufflesuite/ganache/releases). See [documentation](https://www.trufflesuite.com/docs/ganache/quickstart) to understand how to use ganache.


#### Setting up the project

Once the project has been cloned. Start ganache and set up a workspace. Update the truffle-config.js by updating the port and the network_id to the values set up in the work shop. Use template below to understand differences.

```
module.exports = {
  networks: {
      development: {
          host: "127.0.0.1",
          port: <portnumber>,
          network_id: <id>
      },
      ropsten: {
          provider: () => new HDWalletProvider(<private key>, `https://ropsten.infura.io/v3<API Key>`),
          network_id: 3,       // Ropsten's id
          gas: 8000029,       // Ropsten has a lower block limit than mainnet
          confirmations: 2,    // # of confs to wait between deployments. (default: 0)
          //timeoutBlocks: 200,  // # of blocks before a deployment times out  (minimum/default: 50)
          skipDryRun: false     // Skip dry run before migrations? (default: false for public nets )
      },
      solc: {
          optimizer: {
              enabled: true,
              runs: 200
          }
      }
  }
};
```

##### Adding your Managing Wallet

In order to be able to count and add the votes to the contract, you need to set up a wallet address for the transactions to use Ether from. In order to do this you will need to know your private key. 

##### Setting up an Infura Node

Go to https://infura.io/ and create a new infura node. Replace the HTTP provider URL in Api/index.tsx with your own address.

##### Adding Address and ABI

Once config file updated and workspace running, the following series of commands in the terminal within the directory containing the repository.

```
npm rebuild node-sass
npm install
truffle compile
truffle migrate
```

Update both:
- clientSide/exports/contractConfig.js
- contractConfig.ts

Update ABIHomo and ADDRESS_Homo from within build folder. This will require you to ge the ABI and address of the contracts. After updating config settings, navigate to the repository directorary via the Bash Terminal and open two tabs within the directorary, and use the following script commands: 

Within tab one: 
```
npm start
```

Within tab two:
```
npm run dev
```

Open http://localhost:8080/ to see repository running.

## Running the tests
Once you have the project running, within an additional Bash terminal tab execute command:

```
npm test
```

## Verison Strategy

[Semantic versioning](https://semver.org/) has been used.


## Author

* **Jake Chapman** - jc17122@essex.ac.uk
