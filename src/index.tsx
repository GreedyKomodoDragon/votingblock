import sassMiddleware from 'node-sass-middleware';
import path from 'path';
import ReactDOMServer from 'react-dom/server';
import React from 'react';
import App from "./clientSide/components/App";
import { StaticRouter } from "react-router-dom";
import express from 'express';
import APIRouter from './Api/index';
import bodyParser from 'body-parser';
import session from 'express-session';
import Web3 from 'web3';

const server: express.Express = express();

const sess = {
    secret: 'blockchainVoting',
    resave: true,
    saveUninitialized: true,
    cookie: {
        secure: false
    }
};

server.use(session(sess));

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }))

server.use(sassMiddleware({
    src: path.join(__dirname, 'sass'),
    dest: path.join(__dirname, '../public')
}));

server.set('view engine', 'ejs');

server.get('/join', (req, res) => {
    //TODO: check user is not already logged in
    //start up values
    let context = {};

    let publicAddess = "0x";

    if (req.session && req.session.name) {
        res.redirect('/');
    } else {
        const initialMarkup = ReactDOMServer.renderToString(
            <StaticRouter location={req.url} context={context}>
                <App account={publicAddess} />
            </StaticRouter>
        );

        res.render('index', {
            title: "Sign Up!",
            initialMarkup
        })
    }

});

server.get('/', (req, res) => {
    //start up values
    let context = {};

    let publicAddess = "0x";

    if (req.session && req.session.name) {
        publicAddess = req.session.name;
    }

    const initialMarkup = ReactDOMServer.renderToString(
        <StaticRouter location={req.url} context={context}>
            <App account={publicAddess} />
        </StaticRouter>
    );

    res.render('index', {
        title: "Welcome",
        initialMarkup
    })



});

server.get('/createpoll', (req, res) => {
    //start up values
    let context = {};

    let publicAddess = "0x";

    if (req.session && req.session.name) {
        publicAddess = req.session.name;

        const initialMarkup = ReactDOMServer.renderToString(
            <StaticRouter location={req.url} context={context}>
                <App account={publicAddess} />
            </StaticRouter>
        );

        res.render('index', {
            title: "Create Election",
            initialMarkup
        })

    } else {
        res.redirect('/');
    }
});

server.get('/account', (req, res) => {
    if (req.session && req.session.name) {
        //start up values
        let context = {};

        let initialMarkup = ReactDOMServer.renderToString(
            <StaticRouter location={req.url} context={context}>
                <App account={req.session.name} />
            </StaticRouter>
        );

        res.render('index', {
            title: "Account",
            initialMarkup
        })

    } else {
        res.redirect('/');
    }

});

server.get('/account/myelections', (req, res) => {
    if (req.session && req.session.name) {
        //start up values
        let context = {};

        let initialMarkup = ReactDOMServer.renderToString(
            <StaticRouter location={req.url} context={context}>
                <App account={req.session.name} />
            </StaticRouter>
        );

        res.render('index', {
            title: 'My Elections',
            initialMarkup
        })

    } else {
        res.redirect('/');
    }

});

server.get('/account/settings', (req, res) => {
    if (req.session && req.session.name) {
        //start up values
        let context = {};

        let initialMarkup = ReactDOMServer.renderToString(
            <StaticRouter location={req.url} context={context}>
                <App account={req.session.name} />
            </StaticRouter>
        );

        res.render('index', {
            title: 'My Elections',
            initialMarkup
        })

    } else {
        res.redirect('/');
    }

});


server.get('/election/:id', (req, res) => {

    let context = {};


    let publicAddess = "0x";

    if (req.session && req.session.name) {
        publicAddess = req.session.name;
    }


    let initialMarkup = ReactDOMServer.renderToString(
        <StaticRouter location={req.url} context={context}>
            <App account={publicAddess} />
        </StaticRouter>
    );

    res.render('index', {
        title: "Election",
        initialMarkup
    })



});


server.get('/settingup', (req, res) => {

   //start up values
   let context = {};

   let publicAddess = "0x";

   if (req.session && req.session.name) {
       publicAddess = req.session.name;
   }

   const initialMarkup = ReactDOMServer.renderToString(
       <StaticRouter location={req.url} context={context}>
           <App account={publicAddess} />
       </StaticRouter>
   );

   res.render('index', {
       title: "Welcome",
       initialMarkup
   })


});

server.get('/open', (req, res) => {

    let context = {};


    let publicAddess = "0x";

    if (req.session && req.session.name) {
        publicAddess = req.session.name;
    }


    let initialMarkup = ReactDOMServer.renderToString(
        <StaticRouter location={req.url} context={context}>
            <App account={publicAddess} />
        </StaticRouter>
    );

    res.render('index', {
        title: "Election",
        initialMarkup
    })



});


server.get('/forgotpassword', (req, res) => {

    let context = {};


    let publicAddess = "0x";

    if (req.session && req.session.name) {
        publicAddess = req.session.name;
    }


    let initialMarkup = ReactDOMServer.renderToString(
        <StaticRouter location={req.url} context={context}>
            <App account={publicAddess} />
        </StaticRouter>
    );

    res.render('index', {
        title: "Reset Password",
        initialMarkup
    })



});

server.post(['/registration', '/login'], async (req, res) => {

    //use of try-catch greater ensure a reponse can be sent back
    try {
        if (Web3.utils.isAddress(req.body.public_address)) {
            if (req.session) {
                req.session.name = req.body.public_address;
                res.send("successful");
            } else {
                res.send("unsuccessful");
            }

        } else {
            res.send("unsuccessful");
        }
    } catch (error) {
        res.send("unsuccessful");
    }

});

server.get('/logout', (req, res) => {
    if (req.session && req.session.name) {
        req.session.destroy(() => {
            res.redirect('/');
        });
    } else {
        res.redirect('/');
    }
});


server.post('/publicAddress', (req, res) => {
    if (req.session && req.session.name) {
        res.send(req.session.name);
    } else {
        res.send("0x");
    }
});

server.get('/search', (req, res) => {
    let context = {};


    let publicAddess = "0x";

    if (req.session && req.session.name) {
        publicAddess = req.session.name;
    }


    let initialMarkup = ReactDOMServer.renderToString(
        <StaticRouter location={req.url} context={context}>
            <App account={publicAddess} />
        </StaticRouter>
    );

    res.render('index', {
        title: "Election",
        initialMarkup
    })

});

server.get('/resetpassword/:public/:id', (req, res) => {
    let context = {};


    let publicAddess = "0x";

    if (req.session && req.session.name) {
        publicAddess = req.session.name;
    }


    let initialMarkup = ReactDOMServer.renderToString(
        <StaticRouter location={req.url} context={context}>
            <App account={publicAddess} />
        </StaticRouter>
    );

    res.render('index', {
        title: "Reseting Password",
        initialMarkup
    })

});


server.use(express.static('public'));

server.get('*', (req, res) => {
    let context = {};


    let publicAddess = "0x";

    if (req.session && req.session.name) {
        publicAddess = req.session.name;
    }

    let initialMarkup = ReactDOMServer.renderToString(
        <StaticRouter location={'/404'} context={context}>
            <App account={publicAddess} />
        </StaticRouter>
    );

    res.render('index', {
        title: "Page Not Found",
        initialMarkup
    })

});

server.use('/api', APIRouter);

let port  = process.env.PORT ? parseInt(process.env.PORT) : 8080;

server.listen(port, '0.0.0.0', () => {
    console.info('Express listening on port', port);
});