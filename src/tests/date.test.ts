const isValidDateFormat = (date: string) => {
    const dateRegx: RegExp = /^\d{4}(-)(((0)[0-9])|((1)[0-2]))(-)([0-2][0-9]|(3)[0-1])$/;
    return dateRegx.test(date);
};

const numberOfDays = (dayOne: string, dayTwo: string) => {
    const _MS_PER_DAY = 1000 * 60 * 60 * 24;

    const a = new Date(dayOne);
    const b = new Date(dayTwo);

    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

test('Check YYYY-MM-DD is accepted', ()=>{
    expect(isValidDateFormat("2013-12-31")).toBe(true);
});

test('Check YYY-MM-DD is not accepted', ()=>{
    expect(isValidDateFormat("201-12-31")).toBe(false);
});

test('Check Y-MM-DD is not accepted', ()=>{
    expect(isValidDateFormat("1-12-31")).toBe(false);
});

test('Check YYYY-MM-D is not accepted', ()=>{
    expect(isValidDateFormat("2013-12-3")).toBe(false);
});

test('Check YYYY-M-DD is not accepted', ()=>{
    expect(isValidDateFormat("2013-2-31")).toBe(false);
});

test('Number of Days is calcuated within same month', ()=>{
    expect(numberOfDays("2013-12-20", "2013-12-21")).toBe(1);
    expect(numberOfDays("2013-12-20", "2013-12-20")).toBe(0);
    expect(numberOfDays("2013-12-20", "2013-12-19")).toBe(-1);
});

test('Number of Days is calcuated within different months', ()=>{
    expect(numberOfDays("2013-11-20", "2013-12-21")).toBe(31);
});