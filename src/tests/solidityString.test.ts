import Web3 from 'web3';
import { ABIHomo, ADDRESS_Homo, BackendAddress } from '../contractConfig';

const web3 = new Web3(new Web3.providers.HttpProvider('HTTP://127.0.0.1:7545'));
const contract = new web3.eth.Contract(ABIHomo, ADDRESS_Homo);

test('testing the contains', async () => {

    let different = await contract.methods.containsString("one", "two").call({ from: BackendAddress});
    expect(different).toBe(false);

    let multipleWords = await contract.methods.containsString("test", "test word").call({ from: BackendAddress});
    expect(multipleWords).toBe(true);

    let singleLetter = await contract.methods.containsString("t", "test").call({ from: BackendAddress});
    expect(singleLetter).toBe(true);
})