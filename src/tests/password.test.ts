const isValidPassword = (password:string) =>{
    return /[a-z]/g.test(password)
         && /[A-Z]/g.test(password)
         && /[0-9]/g.test(password)
         && /[^a-zA-Z\d]/g.test(password)
         && (password.length >= 8)
}


test('Check password containing all required character, with 9 length', ()=>{
    expect(isValidPassword("Aa&3aaaaa")).toBe(true);
});

test('Check password containing all required character, with 8 length', ()=>{
    expect(isValidPassword("Aa&3aaaa")).toBe(true);
});


test('Check password containing all required character, with 7 length', ()=>{
    expect(isValidPassword("Aa&3aaa")).toBe(false);
})

test('Password Missing special character', ()=>{
    expect(isValidPassword("Aaa3aaa")).toBe(false);
})


test('Password Missing Uppercase character', ()=>{
    expect(isValidPassword("aa&3aaa")).toBe(false);
})

test('Password Missing Uppercase character', ()=>{
    expect(isValidPassword("aa&3aaa")).toBe(false);
})

test('Password Missing Lowercase character', ()=>{
    expect(isValidPassword("AA&3AAA")).toBe(false);
})



