
const getOccurrence = (array: number[], value: number) => {
    let count = 0;
    array.forEach((v) => (v === value && count++));
    return count;
}

const validFPTPVote = (vote: number[], numberOfCandidates: number) => {
    //empty input
    if (vote.length == 0 || numberOfCandidates == 0) {
        return false;
    }

    //Candidates does not match length
    if (vote.length != numberOfCandidates) {
        return false;
    }

    return getOccurrence(vote, 0) === numberOfCandidates - 1 && getOccurrence(vote, 1) === 1;

};

const validBordaVote = (vote: number[], numberOfCandidates: number) => {
    //empty input
    if (vote.length == 0 || numberOfCandidates == 0) {
        return false;
    }

    //Candidates does not match length
    if (vote.length != numberOfCandidates) {
        return false;
    }

    for (let i = 1; i <= numberOfCandidates; i++) {
        if (getOccurrence(vote, i) !== 1) {
            return false;
        }
    }

    return true;

};

test('FPTP: Vote Validation', () => {
    //Has Empty Vote
    expect(validFPTPVote([], 2)).toBe(false);

    //Has 0 Candidiates
    expect(validFPTPVote([0], 0)).toBe(false);

    //Has One candidate but none selected
    expect(validFPTPVote([0], 1)).toBe(false);

    //Has One candidate, one selected
    expect(validFPTPVote([1], 1)).toBe(true);

    //Two candidates, 3 length 
    expect(validFPTPVote([0, 0, 1], 2)).toBe(false);

    //Too many selected
    expect(validFPTPVote([1, 1], 2)).toBe(false);

    //Right amount on 10 candidates
    expect(validFPTPVote([0, 0, 0, 1, 0, 0, 0, 0, 0, 0], 10)).toBe(true);
});

test('Borda: Vote Validation', () => {
    //Has Empty Vote
    expect(validBordaVote([], 2)).toBe(false);

    //Has 0 Candidiates
    expect(validBordaVote([0], 0)).toBe(false);

    //Has One candidate but none selected
    expect(validBordaVote([0], 1)).toBe(false);

    //Has One candidate, one selected
    expect(validBordaVote([1], 1)).toBe(true);

    //Two candidates, 3 length 
    expect(validBordaVote([2, 3, 1], 2)).toBe(false);

    //Two Selected
    expect(validBordaVote([1, 2], 2)).toBe(true);

    //Right amount on 10 candidates
    expect(validBordaVote([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 10)).toBe(true);

    //10 candidates double 2
    expect(validBordaVote([1, 2, 2, 4, 5, 6, 7, 8, 9, 10], 10)).toBe(false);
});
