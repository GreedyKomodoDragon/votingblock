import express from 'express';
import EncryptionRouter from './encryption';
import { ABIHomo, ADDRESS_Homo, ABIAccount, ADDRESS_account, BackendAddress, Private } from '../contractConfig';
import Web3 from 'web3';
import Schedule from 'node-schedule';
import BigNum from 'bignum';


interface ResultsFromBlockchain {
    idElection: number,
    vote: string[],
    owner: string,
    time: number
};

//smart contract Access
//const web3 = new Web3(new Web3.providers.HttpProvider('HTTP://127.0.0.1:7545'));
const web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/v3/4678e389a16b406d94b09d0a62050cc0"));
const contract = new web3.eth.Contract(ABIHomo, ADDRESS_Homo);
const contractAccount = new web3.eth.Contract(ABIAccount, ADDRESS_account);

//Getting Access to account

const account = web3.eth.accounts.privateKeyToAccount("0x" + Private);
web3.eth.accounts.wallet.add(account);
web3.eth.defaultAccount = account.address;
//web3.eth.defaultAccount = "0x0dFB8BD67dD1EfA6d39FC2296D06e6f1FD23086E";

//homomorphic Encryption
const paillier = require('paillier-bignum');
const publicKey = new paillier.PublicKey(process.env.N, process.env.g);
const privateKey = new paillier.PrivateKey(process.env.lambda, process.env.mu, process.env.p, process.env.q, publicKey);

let router = express.Router();

//adds a new result into the database
const dealWithResults = async (_electionID: number, _numberOfCandidates: number, _results: ResultsFromBlockchain[]) => {

    //no one voted in the election
    if (_results.length == 0) {

        contract.methods.submitResults(_electionID, new Array(_numberOfCandidates).fill(0)).estimateGas({ from: web3.eth.defaultAccount })
            .then((result: number) => {
                contract.methods.submitResults(_electionID, new Array(_numberOfCandidates).fill(0)).send({ from: web3.eth.defaultAccount, gas: Math.round(result * 1.5) })
                    .catch((e: any) => {
                        console.log("------------");
                        console.log(e);
                        console.log("------------");
                        console.log("Unable to submit an empty election")
                    })
            }).catch((e: any) => {
                console.log("------");
                console.log("gas price error")
                console.log("------");
            })
    } else {
        let voteTotal: BigNum[] = [];

        //set to zero
        for (let k = 0; k < _numberOfCandidates; k++) {
            voteTotal[k] = publicKey.encrypt(0);
        }

        //add up the votes
        for (let voter = 0; voter < _results.length; voter++) {

            for (let k = 0; k < _numberOfCandidates; k++) {
                const buffer = Buffer.from(_results[voter].vote[k], 'base64');
                const bufString = new BigNum(buffer.toString("hex"), 16);
                voteTotal[k] = publicKey.addition(voteTotal[k], bufString);

            }


        }

        //decrypt the votes to numbers
        let voteUnecrypted:any[] = [];
        for (let i = 0; i < voteTotal.length; i++) {
            voteUnecrypted[i] = privateKey.decrypt(voteTotal[i]).toString();

        }
        console.log("_electionID: " + _electionID);
        console.log("voteUnecrypted: " + voteUnecrypted);

        contract.methods.submitResults(_electionID, voteUnecrypted).estimateGas({ from: web3.eth.defaultAccount })
            .then((result: number) => {
                contract.methods.submitResults(_electionID, voteUnecrypted).send({ from: web3.eth.defaultAccount, gas: Math.round(result * 2) })
                    .then(() => {
                        console.log("Submitted an election result");
                    })
                    .catch((e: any) => {
                        console.log("------------");
                        console.log(e);
                        console.log("------------");
                        console.log("Direct catch: cannot submit results in here")
                    })
            })
            .catch((e: any) =>{
                console.log("------");
                console.log("gas price error")
                console.log("------");
            })
    };
}



//schedule all the jobs
contract.methods.getNumberOfPolls().call({ from: web3.eth.defaultAccount })
    .then((amount: number) => {       
        

        for (let i = 0; i < amount; i++) {
            //getElection
            contract.methods.getElection(i).call({ from: web3.eth.defaultAccount })
                .then(async (election: any[]) => {
                    if (election) {
                        //requires a submission
                        if (election[0] < Math.round((new Date()).getTime() / 1000)) {
                            //check if it has already been submitted
                            let hasSubmittedBefore = await contract.methods.hasResults(i).call({ from: web3.eth.defaultAccount });
                            if (!hasSubmittedBefore) {
                                //Get votes from blockchain
                                contract.methods.getVotes(i).call({ from: web3.eth.defaultAccount })
                                    .then((results: ResultsFromBlockchain[]) => {
                                        dealWithResults(i, election[7].length, results)

                                    })
                                    .catch(() => {
                                        console.log("Unable to submit results");
                                    })
                            }

                        } else {
                            Schedule.scheduleJob(new Date(election[0] * 1000), function () {
                                submitResults(i)
                            });
                        }
                    }
                }).catch((e: any) => {
                    console.log("------------");
                    console.log(e);
                    console.log("------------");
                    console.log("On start: Error in getting election");
                });
        }
    }).catch((e: any) => {
        console.log("------------");
        console.log(e);
        console.log("------------");
        console.log("On start: Error in getting number of elections");
    })

let lastKnownLength: number = 0;
setInterval(() => {

    contract.methods.getNumberOfPolls().call({ from: web3.eth.defaultAccount })
        .catch((e: any) => {
            console.log("------------");
            console.log(e);
            console.log("------------");
            console.log("Error in getting number of elections");
        })
        .then(async (results: string) => {
            //convert the number of elections to an integer
            let resultsInt: number = parseInt(results);

            if (lastKnownLength < resultsInt) {
                //check all of the elections that have not been processed
                for (let i = lastKnownLength; i < resultsInt; i++) {

                    contract.methods.getElection(i).call({ from: web3.eth.defaultAccount })
                        .then(async (election: any[]) => {
                            console.log("Election: " + i);
                            //requires a submission
                            if (election[0] < Math.round((new Date()).getTime() / 1000)) {
                                //check if it has already been submitted
                                let hasSubmittedBefore = await contract.methods.hasResults(i).call({ from: web3.eth.defaultAccount });
                                console.log("Election has result: " + hasSubmittedBefore);
                                if (!hasSubmittedBefore) {
                                    //Get votes from blockchain
                                    contract.methods.getVotes(i).call({ from: web3.eth.defaultAccount })
                                        .then((results: ResultsFromBlockchain[]) => {
                                            dealWithResults(i, election[7].length, results)

                                        })
                                        .catch(() => {
                                            console.log("Unable to submit results");
                                        })
                                }

                            } else {
                                Schedule.scheduleJob(new Date(election[0] * 1000), function () {
                                    submitResults(i)
                                });
                            }
                        }).catch((e: any) => {
                            console.log("------------");
                            console.log(e);
                            console.log("------------");
                            console.log("Error in getting election");
                        })
                }

                lastKnownLength = resultsInt;
            }
        })
}, 60 * 1000);


//Schedule Function
const submitResults = (id: number) => {
    //get the results
    contract.methods.getElection(id).call({ from: web3.eth.defaultAccount })
        .then((election: (string | any[])[]) => {
            if (election) {
                contract.methods.getVotes(id).call({ from: web3.eth.defaultAccount })
                    .then((results: ResultsFromBlockchain[]) => {
                        dealWithResults(id, election[7].length, results);
                    }).catch(() => {
                        console.log("Votes could not be recieved")
                    })

            }
        }).catch(() => {
            console.log("Election was unable to be gathered")
        })
}

router.use('/encryption', EncryptionRouter);

export default router;
