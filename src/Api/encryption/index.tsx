import express from 'express';
import BigNum from 'bignum';

// encryption set up
const paillier = require('paillier-bignum');
const publicKey = new paillier.PublicKey(process.env.N, process.env.g);
const privateKey = new paillier.PrivateKey(process.env.lambda, process.env.mu, process.env.p, process.env.q, publicKey);

let router = express.Router();

router.post('/homomorphicEncrypt', (req, res) => {
    if (req.body.votes) {
        let homomorphicVotes: string[] = [];
        for (let i = 0; i < req.body.votes.length; i++) {
            homomorphicVotes[i] = Buffer.from(publicKey.encrypt(req.body.votes[i]).toString(16), 'hex').toString('base64');
        }
        res.send(homomorphicVotes);
    }else{
        res.send("unsuccessful");
    }
});

router.post('/homomorphicDecrypt', (req, res) => {
    let voteTotal: number[] = [];

    //set to zero
    for (let k = 0; k < req.body.votes[0].length; k++) {
        voteTotal[k] = publicKey.encrypt(0);
    }


    //add up the votes
    req.body.votes.forEach((element: string[]) => {
        for (let i = 0; i < element.length; i++) {
            const buffer = Buffer.from(element[i], 'base64');
            const bufString = new BigNum(buffer.toString("hex"), 16);
            voteTotal[i] = publicKey.addition(voteTotal[i], bufString);
        }
    });
    //decrypt the votes to numbers
    let voteUnecrypted = [];
    for (let i = 0; i < voteTotal.length; i++) {
        voteUnecrypted[i] = privateKey.decrypt(voteTotal[i]).toString();
    }

    res.send(voteUnecrypted);

});

export default router;

