import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import {BrowserRouter} from "react-router-dom";
import Axios from 'axios';

Axios.post('/publicAddress').then((result)=>{
    ReactDOM.hydrate(
        <BrowserRouter>
            <App account={result.data}/>
        </BrowserRouter>,
        document.getElementById('root')
    );    
});



