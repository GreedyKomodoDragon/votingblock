import React, { useRef, useState, useEffect } from 'react';
import LoadingIcon from './LoadingIcon';
import Web3 from 'web3';
import { TiTick } from 'react-icons/ti';
import { MdClose } from 'react-icons/md';

interface ForgotPassword {
    getAddress: Function,
    isValidPassword: Function,
    contractAccount: any
}


const ForgotPassword = (props: ForgotPassword) => {

    useEffect(() => {
        props.getAddress().then((result: string) => {
            setAddress(result);
        });

        setInterval(() => {
            props.getAddress().then((result: string) => {
                if (result != address) {
                    setAddress(result);
                }
            });
        }, 1000);

    }, []);


    //references 
    const yourAddress = useRef<HTMLInputElement>(null);
    const answer = useRef<HTMLInputElement>(null);
    const passwordOne = useRef<HTMLInputElement>(null);
    const passwordTwo = useRef<HTMLInputElement>(null);

    //state
    const [loading, setLoading] = useState<boolean>(false);
    const [address, setAddress] = useState<string>("");
    const [question, setQuestion] = useState<string>("What was the name of your second pet?");
    const [errorMessage, setErrorMessage] = useState<string>("");
    const [stage, setStage] = useState<number>(0);

    //button disabled
    const [buttonDisabled, setButtonDisabled] = useState<boolean>(false);



    //password requirements
    const [hasUppercase, setHasUppercase] = useState<boolean>(false);
    const [hasLowercase, setHasLowercase] = useState<boolean>(false);
    const [hasSpecial, setHasSpecial] = useState<boolean>(false);
    const [hasDigit, setHasDigit] = useState<boolean>(false);
    const [eightLetters, setEightLetters] = useState<boolean>(false);

    return (
        <div className={"row"}>
            {
                stage == 2 ?
                    <div className={"columnsize-12"}>

                        <h1 className={"centre middle"}>Password has been reset</h1>
                    </div>
                    :
                    stage == 1 ?
                        <div className={"columnsize-12"}>
                            <h2>Your Security Question for {address}:</h2>
                            <h3>{question}</h3>
                            <div className={"row"}>
                                <input ref={answer} className={"inputBox-100"} />
                                <h2>Your New Password:</h2>
                                <input type={"password"} ref={passwordOne} onChange={() => {
                                    if (passwordOne.current) {
                                        setHasLowercase(/[a-z]/g.test(passwordOne.current.value));
                                        setHasSpecial(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g.test(passwordOne.current.value));
                                        setHasUppercase(/[A-Z]/g.test(passwordOne.current.value));
                                        setHasDigit(/[0-9]/g.test(passwordOne.current.value));
                                        setEightLetters((passwordOne.current.value.length >= 8));
                                    }
                                }} className={"inputBox-100"} />
                                <input type={"password"} ref={passwordTwo} className={"inputBox-100"} />
                                <div className="requirements">
                                    <p className={hasUppercase ? "valid" : "invalid"}>{hasUppercase ? <TiTick /> : <MdClose />} One Uppercase Character</p>
                                    <p className={hasLowercase ? "valid" : "invalid"}>{hasLowercase ? <TiTick /> : <MdClose />}  One Lowercase Character</p>
                                    <p className={hasSpecial ? "valid" : "invalid"}>{hasSpecial ? <TiTick /> : <MdClose />}  One Special Character</p>
                                    <p className={hasDigit ? "valid" : "invalid"}>{hasDigit ? <TiTick /> : <MdClose />}  One Digit Character</p>
                                    <p className={eightLetters ? "valid" : "invalid"}>{eightLetters ? <TiTick /> : <MdClose />}  At least 8 Character</p>

                                </div>
                            </div>

                            {
                                loading ?
                                    <div>
                                        <h3>Currently Processing Transaction</h3>
                                        <LoadingIcon />
                                    </div> :
                                    null
                            }
                            {
                                errorMessage ?
                                    <div className="error">
                                        {errorMessage}
                                    </div>
                                    :
                                    null
                            }

                            <button disabled={buttonDisabled} onClick={async () => {
                                setButtonDisabled(true);

                                //checks passwords exist and match
                                if (passwordOne.current && passwordTwo.current) {
                                    if (passwordOne.current.value !== passwordTwo.current.value || !props.isValidPassword(passwordOne.current.value)) {
                                        //some error message
                                        setErrorMessage("invalid password");
                                        setButtonDisabled(false);
                                    } else if (!answer.current || !answer.current.value) {
                                        //some error message about not providing an answer
                                        setErrorMessage("provide an answer");
                                        setButtonDisabled(false);
                                    } else {
                                        setLoading(true);
                                        if (props.contractAccount) {
                                            props.contractAccount.methods.changePassword(Web3.utils.soliditySha3(answer.current.value), Web3.utils.soliditySha3(passwordOne.current.value)).send({ from: address })
                                                .on('receipt', () => {
                                                    setLoading(false);
                                                    setStage(2);
                                                    setButtonDisabled(false);
                                                })
                                                .catch(() => {
                                                    setLoading(false);
                                                    setErrorMessage("Answer was wrong")
                                                    setButtonDisabled(false);
                                                });
                                        }

                                    }

                                }
                            }} className="sendLinkButton">Submit Password</button>
                        </div>
                        :
                        <div className={"columnsize-12"}>
                            <h2>Resetting Your Password</h2>
                            <h5>Provide the public address you have signed up with and we'll present your security question to answer</h5>
                            <h4>Provide Your Public Address</h4>
                            {
                                errorMessage ?
                                    <div className="error">
                                        {errorMessage}
                                    </div>
                                    :
                                    null
                            }
                            <div className={"div-60"}>
                                <input type="text" ref={yourAddress} className="inputBox-100" value={address} placeholder="Getting Public Address..." disabled />
                            </div>
                            <div>
                                <button onClick={async () => {
                                    //show user form is processing

                                    //public address must exist and is valid
                                    if (yourAddress.current && Web3.utils.isAddress(yourAddress.current.value) && props.contractAccount) {
                                        //check if public address is part of our address range
                                        let addressFound = yourAddress.current.value;

                                        props.contractAccount.methods.isRegistered(addressFound).call({ from: addressFound })
                                            .then((registered: any) => {
                                                if (registered && props.contractAccount) {
                                                    props.contractAccount.methods.getSecurityQuestion().call({ from: addressFound })
                                                        .then((question: string) => {
                                                            setQuestion(question);
                                                            setStage(1);

                                                        }).catch(() => {
                                                            setErrorMessage("A metamask-contract error occured, try again in a minute");
                                                        })
                                                } else {
                                                    setErrorMessage("Public Address has not been registered, we recommend you use Metamask to autofill to avoid any mistakes");
                                                }
                                            });
                                    } else {
                                        setErrorMessage("Invalid Address Provided, we recommend you use Metamask to autofill to avoid any mistakes");
                                    }

                                }} className="sendLinkButton">Request Question</button>
                            </div>
                        </div>
            }
         
            
        </div>
    )

}

export default ForgotPassword;
