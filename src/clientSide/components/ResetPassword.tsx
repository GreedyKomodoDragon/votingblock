import React, { useState } from 'react';
import { useEffect } from 'react';
import Axios from 'axios';
import LoadingIcon from './LoadingIcon';

interface ResetPassword {
    resetID: String,
    account: String
}


const ResetPassword = (props: ResetPassword) => {
    //state
    const [loading, setLoading] = useState<boolean>(true);
    const [valid, setValid] = useState<boolean>(false);

    const [passwordOne, setPasswordOne] = useState<string>("");
    const [passwordTwo, setPasswordTwo] = useState<string>("");
    const [error, setError] = useState<string>("");

    //valid password
    const isValidPassword = (password: string) => {
        return /[a-z]/g.test(password)
            && /[A-Z]/g.test(password)
            && /[0-9]/g.test(password)
            && /[^a-zA-Z\d]/g.test(password)
            && (password.length >= 8)
    }


    useEffect(() => {
        Axios.post('/api/user/resetpassword', {
            public: props.account,
            resetCode: props.resetID
        })
            .then(response => {
                if (response.data && response.data == "successful") {
                    setValid(true)
                }
                setLoading(false);
            })
            .catch(err => {

            });

    });

    const handleSubmit = () => {
        if (passwordOne.length > 0 && passwordTwo.length > 0) {
            if (passwordOne == passwordTwo) {
                if (isValidPassword(passwordOne)) {
                    //Send Update Axios Route

                    Axios.post('/api/user/updatePassword', {
                        passwordOne: passwordOne,
                        passwordTwo: passwordTwo,
                        resetID: props.resetID
                    })
                        .then(response => {
                            if (response.data == "successful") {
                                window.location.pathname = '/';
                            } else {
                                setError("password could not be changed");
                            }
                        })
                        .catch(err => { })

                } else {
                    setError("Password much follow rules set out below (Not yet placed below)");
                }
            } else {
                setError("passwords do not match");
            }

        } else {
            setError("Must provide passwords in both fields")
        }
    }

    return (
        <div className={"row"}>
            {
                loading ?
                    <LoadingIcon />
                    :
                    valid ?
                        <div className="columnsize-12">
                            <h3>Enter a New Password for {props.account}</h3>
                            {
                                error ?
                                    <div className="error">
                                        {error}
                                    </div>
                                    :
                                    null
                            }
                            <div className="resetForm">
                                <h4>Enter Password</h4>
                                <input className="inputBox-60" onChange={(e) => {
                                    setPasswordOne(e.currentTarget.value);
                                }} type="password" />
                                <h4>Confirm Password</h4>
                                <input className="inputBox-60" onChange={(e) => {
                                    setPasswordTwo(e.currentTarget.value);
                                }} type="password" />
                                <div>
                                    <button onClick={() => handleSubmit()}>Submit New Password</button>
                                </div>


                            </div>
                        </div>
                        :
                        <div>
                            <h3>Reset Link has expired, or link could be invalid</h3>
                        </div>
            }
        </div>
    )

}

export default ResetPassword;
