import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import LoadingIcon from './LoadingIcon';
import Web3 from 'web3';
import { Doughnut, Line } from 'react-chartjs-2';
import { ElectionWithCandidates, candidateWithVote, graphData, candidate } from '../exports/interfaces';
import Donation from './Ballot/Donation';
import BallotFirstPost from './Ballot/BallotFirstPost';
import BallotBorda from './Ballot/BallotBorda';



interface ElectionProps {
  account: string
  electionID: number,
  convertTime(unix: number): string,
  convertRGBToHex: Function,
  contractMain: any
}

interface StringToNumber {
  [key: string]: number;
}

interface Vote {
  idElection: number,
  vote: string[];
  owner: string;
  time: number;
}

const Election = (props: ElectionProps) => {

  //state
  const [election, setElection] = useState<ElectionWithCandidates>();
  const [description, setDescription] = useState<string>();
  const [primativeVotes, setPrimativeVotes] = useState<number[]>([]);
  const [votes, setVotes] = useState<candidateWithVote[]>();
  const [data, setData] = useState<graphData>();
  const [error, setError] = useState<string>("");
  const [dataLine, setDataLine] = useState<any>("");

  //loading
  const [loading, setLoading] = useState<boolean>(false);


  useEffect(() => {
    //Get the data from the contract
    if (props.contractMain && props.account) {
      console.log("in here");
      props.contractMain.methods.getElection(props.electionID).call({ from: props.account })
        .then(async (electionFound: any[]) => {
          if (props.contractMain) {
            let candidates: candidate[] = [];
            let prim = new Array(electionFound[7].length).fill(0);
            setPrimativeVotes(prim)

            for (let i = 0; i < electionFound[6].length; i++) {
              candidates.push({ id: i, name: electionFound[6][i], party: electionFound[7][i], colour: props.convertRGBToHex(electionFound[8][i], electionFound[9][i], electionFound[10][i]) })
            }

            let ElectionWithCandidatesArray: ElectionWithCandidates = { id: props.electionID, nameOfElection: electionFound[1], endDate: electionFound[0], candidates: candidates, type: electionFound[11] }
            let descriptionFound: string = await props.contractMain.methods.getElectionDescription(props.electionID).call({ from: props.account });
            setDescription(descriptionFound);

            //set the election state
            setElection(ElectionWithCandidatesArray);

            //check if election has ended
            if (electionFound[0] < Math.round((new Date()).getTime() / 1000)) {
              //get the results
              let results = await props.contractMain.methods.getResults(props.electionID).call({ from: props.account });
              let candidatesWithResults: candidateWithVote[] = [];

              for (let i = 0; i < results.length; i++) {
                candidatesWithResults.push({ id: i, name: electionFound[6][i], numberOfVotes: results[i], party: electionFound[7][i], colour: props.convertRGBToHex(electionFound[8][i], electionFound[9][i], electionFound[10][i]) });
              }

              let labels: string[] = [];
              let data: number[] = [];
              let color: string[] = [];

              candidatesWithResults.forEach(element => {
                labels.push(element.party);
                data.push(element.numberOfVotes);
                color.push(element.colour)
              });


              let graphData: graphData = {
                labels: labels,
                datasets: [{
                  label: '# of Votes',
                  data: data,
                  backgroundColor: color,
                  borderColor: color,
                  borderWidth: 1
                }]
              };

              setData(graphData);
              setVotes(candidatesWithResults);

              //Get the votes
              let end = electionFound[0];
              let start = electionFound[2];

              let daylist: StringToNumber = getDaysArray(new Date(start * 1000), new Date(end * 1000));

              let votes: Vote[] = await props.contractMain.methods.getVotes(props.electionID).call({ from: props.account });

              for (let i = 0; i < votes.length; i++) {
                let key: string = convertUnixToDate(votes[i].time);

                if (!daylist[key]) {
                  daylist[key] = 1
                } else {
                  daylist[key]++;
                }
              }

              let dataForLine = {
                labels: Object.keys(daylist),
                datasets: [
                  {
                    label: 'Votes',
                    fill: false,
                    lineTension: 1,
                    backgroundColor: 'rgba(75,192,192,0.4)',
                    borderColor: 'rgba(75,192,192,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: Object.values(daylist)
                  }
                ]
              };
              setDataLine(dataForLine);
            }
          }
        });
    }



  }, [props.contractMain]);

  const getDaysArray = (start: Date, end: Date) => {
    let datesFound: StringToNumber = {};

    for (let dt = start; dt <= end; dt.setDate(dt.getDate() + 1)) {
      let newDate: Date = new Date(dt);
      let key: string = newDate.getDate() + "-" + newDate.getMonth() + "-" + newDate.getFullYear()
      datesFound[key] = 0;
    }
    return datesFound;

  };

  const convertUnixToDate = (unix: number) => {
    let newDate = new Date(unix * 1000);
    return newDate.getDate() + "-" + newDate.getMonth() + "-" + newDate.getFullYear();
  };


  const getOccurrence = (array: number[], value: number) => {
    let count = 0;
    array.forEach((v) => (v === value && count++));
    return count;
  };

  const validFPTPVote = (vote: number[], numberOfCandidates: number) => {
    //empty input
    if (vote.length == 0 || numberOfCandidates == 0) {
      return false;
    }

    //Candidates does not match length
    if (vote.length != numberOfCandidates) {
      return false;
    }

    return getOccurrence(vote, 0) === numberOfCandidates - 1 && getOccurrence(vote, 1) === 1;

  };

  const validBordaVote = (vote: number[], numberOfCandidates: number) => {
    //empty input
    if (vote.length == 0 || numberOfCandidates == 0) {
      return false;
    }

    //Candidates does not match length
    if (vote.length != numberOfCandidates) {
      return false;
    }

    for (let i = 1; i <= numberOfCandidates; i++) {
      if (getOccurrence(vote, i) !== 1) {
        return false;
      }
    }

    return true;

  };

  const handleSubmit = () => {
    if (props.account !== "0x") {
      if (election && primativeVotes) {

        //check the votes are valid for the system
        let validVote = true;
        if (election.type == 0) {
          validVote = validFPTPVote(primativeVotes, election.candidates.length);
        } else {
          validVote = validBordaVote(primativeVotes, election.candidates.length);
        }

        if (validVote) {
          Axios.post('/api/encryption/homomorphicEncrypt/', {
            votes: primativeVotes
          })
            .then((result) => {
              let voteHomomorphic = result.data;

              //submit vote to the election
              setLoading(true);
              if (props.contractMain) {
                props.contractMain.methods.addVote(election.id, voteHomomorphic).send({ from: props.account })
                  .on('receipt', () => {
                    setLoading(false);
                    window.location.pathname = '/account';
                  })
                  .on('error', () => {
                    setLoading(false);
                    setError("Metamask was unable to submit vote");
                  });
              }

            })
            .catch(err => {
              setError("Vote has not been recorded as unable to encrypt your vote at this time");
            })
        } else {
          if (election.type == 1) {
            setError("Ballot has not been casted correctly, duplicate numbers used");
          } else {
            setError("Ballot has not been casted correctly, must select a candidate");
          }
        }
      }
    } else {
      setError("Please Log in to be able vote");
    }
  }

  return (
    <div>
      {
        error
          ?
          <div className={"error"}>{error}</div>
          : null
      }
      {election ?
        <div className={"row"}>
          <div className={"columnsize-12"}>
            <h1>Election: {Web3.utils.toUtf8(election.nameOfElection).trim()}</h1>
          </div>
          <div className={"columnsize-6"}>
            <h2>Description: </h2>
            <h3 className={"descriptionElection"}>{description}</h3>
          </div>
          <div className={"columnsize-6"}>
            <h2>End Date: {props.convertTime(election.endDate)}</h2>
          </div>
          <div className={"columnsize-12"}>
            <h3>How to vote:</h3>
            {
              election.type == 0 ?
                <ul>
                  <li>This election uses the single vote implementation</li>
                  <li>Pick your favourite candidate by clicking the box next to the candidiate</li>
                  <li>You can keep changing until you submit the ballot</li>
                </ul>
                :
                <ul>
                  <li>This election uses the Ranking voting method</li>
                  <li>Where {election.candidates.length} is the best and 1 is the worst you can give a candidate</li>
                  <li>Rank every candidate from {election.candidates.length} to 1, no duplicate numbers can be used e.g. cannot use {election.candidates.length} twice</li>
                </ul>
            }
          </div>
          {
            votes ?
              <div className={"columnsize-12"}>
                <Doughnut data={data} />
                <div className={"row"}>
                  <div className={"columnsize-6"}>
                    <h3>Vote Breakdown:</h3>
                    <table className={"width90"}>
                      <thead>
                        <tr>
                          <th>Party</th>
                          <th>Candidate</th>
                          <th>Number of votes</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          votes.map((value: candidateWithVote, index: number) => {
                            return (
                              <tr key={index}>
                                <td>{value.name}</td>
                                <td>{value.party}</td>
                                <td>{value.numberOfVotes}</td>
                              </tr>);
                          })
                        }
                      </tbody>
                    </table>
                  </div>
                  <div className={"columnsize-6"}>
                    <h3>Vote Over Time:</h3>
                    {dataLine ?
                      <Line data={dataLine} />
                      :
                      <LoadingIcon />
                    }
                  </div>
                </div>
              </div>
              :
              <div>

                <table className={"ballot"}>
                  <thead>
                    <tr>
                      <th>Party</th>
                      <th>Vote</th>
                    </tr>
                  </thead>
                  {
                    election.type == 0 ?
                      <BallotFirstPost election={election} setPrimativeVotes={setPrimativeVotes} />
                      :
                      <BallotBorda election={election} primativeVotes={primativeVotes} setPrimativeVotes={setPrimativeVotes} />
                  }
                </table>
                <div className={"columnsize-12"}>
                  {
                    loading ?
                      <div>
                        <h3>Currently Processing Transaction</h3>
                        <LoadingIcon />
                      </div>
                      : null
                  }
                  <input type="button" onClick={handleSubmit} className={"ballot-button"} value="Submit Ballot" />
                </div>
                <Donation contractMain={props.contractMain} account={props.account} electionID={"" + props.electionID} />
              </div>
          }
        </div>
        : <div className={"columnsize-12"}><h3>Loading...</h3><LoadingIcon /></div>
      }
    </div >
  )
}

export default Election;
