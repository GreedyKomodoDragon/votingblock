
import React from 'react';

interface LoadingIcon {

}

const LoadingIcon = (props: LoadingIcon) => {
    return (
        <div className="loader">Loading...</div>
    );
};


export default LoadingIcon;