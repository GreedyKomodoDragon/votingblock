import React, { useEffect, useRef } from 'react';


interface IdenticonProps {
    size: number,
    account: string
}


const Identicon = (props: IdenticonProps) => {
    const canvasRef = useRef<HTMLCanvasElement>(null);

    useEffect(() => {
        createHex();
    })


    const addZeros = (hexFrag:string) => {
        var zeros = new Array(hexFrag.length).join('0');
        return (zeros + hexFrag).slice(-hexFrag.length);
    }

    const invertColour = (hex: string) => {
        //remove hash
        if (hex.indexOf('#') === 0) {
            hex = hex.slice(1);
        }

         // invert color components
        let r = (255-parseInt(hex.slice(0, 2), 16)).toString(16);
        let g = (255-parseInt(hex.slice(2, 4), 16)).toString(16);
        let b = (255-parseInt(hex.slice(4, 6), 16)).toString(16);

        // pad each with zeros and return
        return "#" + addZeros(r) + addZeros(g) + addZeros(b);

    }


    const createHex = () => {
        const count: number = 5;
        const block: number = Math.floor(props.size / count);
        const hashcolor: string = props.account.slice(2, 8);

        if (canvasRef.current) {
            canvasRef.current.width = block * 5;
            canvasRef.current.height = block * 5;

            let blockOnOffArr: boolean[] = [];
            for (let i = 2; i < props.account.length; i++) {
                if (parseInt(props.account.charAt(i), 16) < 8) {
                    blockOnOffArr[i] = false;
                } else {
                    blockOnOffArr[i] = true;
                }
            }

            let map: boolean[][] = [];
            map[0] = map[4] = blockOnOffArr.slice(0, 5)
            map[1] = map[3] = blockOnOffArr.slice(5, 10)
            map[2] = blockOnOffArr.slice(10, 15)

            const twoDim: any = canvasRef.current.getContext('2d');

            if (twoDim) {
                twoDim.imageSmoothingEnabled = false;
                twoDim.clearRect(0, 0, canvasRef.current.width, canvasRef.current.height);

                let i: number = 0;
                for (let row of map) {
                    let j: number = 0;
                    for (let square of row) {

                        twoDim.fillStyle = square ? "#" + hashcolor : invertColour("#" + hashcolor);
                        twoDim.fillRect(block * i, block * j, block, block);
                        j++;
                    }
                    i++;
                }
            }
        }
    }
    return (
        <canvas ref={canvasRef} style={{ width: props.size, height: props.size }} />
    );
};


export default Identicon;
