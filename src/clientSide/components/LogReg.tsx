
import React, { useEffect, useState } from 'react';
import Login from './login';
import Registration from './registration';



interface LogReg {
    getAddress: Function
    isValidPassword: Function
    contractAccount: any
}

const LogReg = (props: LogReg) => {
    

    const [address, setAddress] = useState<string>("");

    useEffect(() => {
        props.getAddress().then((result: string) => {
            setAddress(result);
        });

        setInterval(() => {
            props.getAddress().then((result: string) => {
                if (result != address) {
                    setAddress(result);
                }
            });
        }, 1000);

    }, []);


    return (
        <div className={"row"}>
            <Registration contractAccount={props.contractAccount} isValidPassword={props.isValidPassword} address={address}   />
            <Login contractAccount={props.contractAccount} isValidPassword={props.isValidPassword} address={address}  />
        </div>
    );


};


export default LogReg;