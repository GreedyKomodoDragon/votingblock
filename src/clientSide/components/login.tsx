import React, { useRef, useState } from 'react';
import Axios from 'axios';
import Web3 from 'web3';


interface LoginProps {
    isValidPassword: Function
    address: string
    contractAccount: any
}


const Login = (props: LoginProps) => {
    //references
    const login: React.RefObject<HTMLInputElement> = useRef<HTMLInputElement>(null); //used to get public address
    const pass: React.RefObject<HTMLInputElement> = useRef<HTMLInputElement>(null);  //used to get password

    //state
    const [errorPassword, setErrorPassword] = useState<string>("");
    const [errorPublic, setErrorPublic] = useState<string>("");

    //button disabled
    const [buttonDisabled, setButtonDisabled] = useState<boolean>(false);


    //Function: handleSubmit @params: event: React.FormEvent<HTMLFormElement>
    //Purpose Validations Login Details then sends data to the server
    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        setButtonDisabled(true);
        event.preventDefault();

        let error = false;

        //checks for password existance and is valid
        if (!pass.current || !pass.current.value || !props.isValidPassword(pass.current.value)) {
            error = true;
            setButtonDisabled(false);
            setErrorPassword("Provide a valid password");
        } else {
            setErrorPassword("");
        }


        //if errors are present don't go to server
        if (!error && pass.current) {

            if (props.address && props.contractAccount) {
                props.contractAccount.methods.login(Web3.utils.soliditySha3(pass.current.value)).call({ from: props.address })
                    .then((() => {
                        Axios.post('/login', {
                            public_address: props.address
                        }).then((result) => {
                            if (result.data != "unsuccessful") {
                                window.location.pathname = '/account';
                            } else {
                                setErrorPublic("Server was unable deal with the request");
                            }
                        })
                        setButtonDisabled(false);
                    })).catch(() => {
                        setButtonDisabled(false);
                        setErrorPublic("Public Address or Password is incorrect");
                    })
            }

        } else {
            //unexpected errors        
            setButtonDisabled(false);    
            window.alert("Unexpected Error");
        }

    }





    return (
        <div className={"columnsize-6 login-form"}>
            <h3>Welcome Back! Sign in here</h3>
            <form onSubmit={handleSubmit} action={"/"}>
                <div>
                    <h5>Public Address (Autofilled)</h5>
                    <input type="text" className="inputBox" placeholder="Getting Public Address...." value={props.address} disabled />
                    {props.address == "0x" ?
                        <div className="error">
                            Check Metamask is connected and been given Privileges
                    </div>
                        : null}
                </div>
                {errorPublic ?
                    <div className="error">
                        {errorPublic}
                    </div> :
                    null}
                <div>
                    <input type="password" ref={pass} className="inputBox" placeholder="Your Password *" />
                </div>
                {errorPassword ?
                    <div className="error">
                        {errorPassword}
                    </div> :
                    null}
                <div>
                    <input disabled={buttonDisabled} type="submit" className="btnSubmit" />
                </div>
                <div>
                    <a href="/forgotpassword" className="ForgetPwd">Forget Password?</a>
                </div>
            </form>
        </div>
    )

}

export default Login;
