import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import Navbar from './Navbar';
import { useEffect } from 'react';
import LogReg from './LogReg';
import CreatePoll from './CreatePoll';
import Account from './Account';
import Election from './Election';
import Search from './Search';
import OpenElections from './OpenElections';
import ForgotPassword from './ForgotPassword';
import Web3 from 'web3';
import NotFound from './NotFound';
import Landing from './Landing';
import Instructions from './Instructions';
import { useState } from 'react';
const config = require('../exports/contractConfig.js');

interface AppProps {
    account: string
}

//extending the existing window to support moderm browsers
declare global {
    interface Window { ethereum: any; }
}

const App = (props: AppProps) => {

    const [contractMain, setContractMain] = useState<any>();
    const [contractAccount, setContractAccount] = useState<any>();


    useEffect(() => {
        window.ethereum.enable().then(() => {
            const web3: Web3 = new Web3(Web3.givenProvider); //connect to metaMask
            const contractCreated:any = new web3.eth.Contract(config.ABIHomo, config.ADDRESS_Homo);
            const contractAccount:any = new web3.eth.Contract(config.ABIAccount, config.ADDRESS_account);

            setContractMain(contractCreated);
            setContractAccount(contractAccount)
        });

    }, []);

    const convertTime = (unix: number) => {
        const a = new Date(unix * 1000);
        //string months
        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        const year = a.getFullYear();
        const month = months[a.getMonth()];
        const date = a.getDate();
        let hour = a.getHours() + "";
        let min = a.getMinutes() + "";
        let sec = a.getSeconds() + "";
        if (hour.length == 1) {
            hour = "0" + hour;
        }
        if (min.length == 1) {
            min = "0" + min;
        }
        if (sec.length == 1) {
            sec = "0" + sec;
        }
        const time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
        return time;
    }

    const getAddress = async () => {

        const web3 = new Web3(Web3.givenProvider || "http://local:8545"); //connect to metaMask

        if (window.ethereum) {
            try {

                await window.ethereum.enable(); //ask permisson
                const accounts: string[] = await web3.eth.getAccounts();
                console.log(accounts[0]);
                return accounts[0];

            } catch (e) {
                return "0x";
            }
        } else {
            return "0x";
        }
    }

    const componentToHex = (d: number) => {
        let hex: string = d.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    const convertRGBToHex = (r: string, g: string, b: string) => {
        let hex: string = "#" + componentToHex(parseInt(r)) + componentToHex(parseInt(g)) + componentToHex(parseInt(b));
        return hex;
    }

    const convertHexColourToRGB = (hex: string) => {
        let hexArray: RegExpExecArray | null = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

        //[0] == #
        return hexArray ?
            [parseInt(hexArray[1], 16),
            parseInt(hexArray[2], 16),
            parseInt(hexArray[3], 16)]
            : null

    }

    //valid password
    const isValidPassword = (password: string) => {
        return /[a-z]/g.test(password)
            && /[A-Z]/g.test(password)
            && /[0-9]/g.test(password)
            && /[^a-zA-Z\d]/g.test(password)
            && /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g.test(password)
            && (password.length >= 8)
    }



    return (
        <div>
            <div className={"container"}>
                <Navbar account={props.account} />
                <div className={"nav-spacer"}></div>
                <Switch>
                    <Route exact={true} path={'/'} render={() => (
                        <Landing />
                    )} />
                    <Route exact={true} path={'/join'} render={() => (
                        <div>
                            <div className={"nav-spacer"} />
                            <LogReg contractAccount={contractAccount} isValidPassword={isValidPassword} getAddress={getAddress} />
                            <div className={"nav-spacer"} />
                        </div>
                    )} />
                    <Route exact={true} path={'/createpoll'} render={() => (
                        <CreatePoll contractMain={contractMain} convertRGBToHex={convertRGBToHex} convertHexToRGB={convertHexColourToRGB} account={props.account} />
                    )} />
                    <Route path={'/account'} render={() => (
                        <Account contractMain={contractMain} contractAccount={contractAccount} convertRGBToHex={convertRGBToHex} convertTime={convertTime} account={props.account} />
                    )} />
                    <Route exact={true} path={'/election/:id'} render={({ match }) => (
                        <Election contractMain={contractMain} convertRGBToHex={convertRGBToHex} convertTime={convertTime} account={props.account} electionID={match.params.id} />
                    )} />
                    <Route exact={true} path={'/search/'} render={({ match }) => (
                        <Search contractMain={contractMain} account={props.account} convertTime={convertTime} />
                    )} />
                    <Route exact={true} path={'/open'} render={({ match }) => (
                        <OpenElections contractMain={contractMain} account={props.account} />
                    )} />
                    <Route exact={true} path={'/forgotpassword'} render={({ match }) => (
                        <ForgotPassword contractAccount={contractAccount} isValidPassword={isValidPassword} getAddress={getAddress} />
                    )} />
                    <Route exact={true} path={'/settingup'} render={({ match }) => (
                        <Instructions />
                    )} />
                    <Route exact={true} path={'/404'} render={() => (
                        <NotFound />
                    )} />
                    <Route render={() => <Redirect to="/404" />} />
                </Switch>
                <div className={"nav-spacer"}></div>
            </div>
        </div>
    );


};


export default App;
