import React, { useEffect, useState } from 'react';
import ElectionItem from './OpenElections/ElectionItem';
import LoadingIcon from './LoadingIcon';
import { latest } from '../exports/interfaces';
import Web3 from 'web3';



interface IOpenProps {
    account: string,
    contractMain: any
}

const OpenElections = (props: IOpenProps) => {
    const [lastestElections, setLastestElections] = useState<latest[]>();

    useEffect(() => {


        if (!lastestElections) {
            //Use Web3 to access contract
            if (props.account != "0x") {
                if (props.contractMain) {
                    props.contractMain.methods.getNumberOfPolls().call({ from: props.account })
                        .then(async (numberOfElection: number) => {
                            if (numberOfElection > 0) {
                                let electionList: latest[] = [];
                                let [i, k] = [numberOfElection - 1, 0];
                                while (i > -1 && k < 10) {
                                    if(props.contractMain){
                                        let election = await props.contractMain.methods.getElection(i).call({ from: props.account });
                                        let description = await props.contractMain.methods.getElectionDescription(i).call({ from: props.account });
    
                                        if (election) {
                                            electionList.push({ numberOfCandidates: election[6].length, name: election[1], id: i + "", endDate: election[0], owner: election[3], description: description });
                                        }
                                        i--;
                                        k++;
                                    }else{
                                        break;
                                    }
                                    
                                }
                                setLastestElections(electionList);
                            }
                        });
                }

            } else {
                // display some SVG later on
            }
        }
    }
    );

    return (
        <div className={"row"}>
            <div className={"columnsize-12"}>
                <h2>Latest Elections Created</h2>
            </div>
            {props.account != "0x" ?
                lastestElections ?
                    lastestElections.map((item, key) =>
                        <div className={"columnsize-6"} key={key}>
                            <ElectionItem description={item.description} account={props.account} name={Web3.utils.toUtf8(item.name)} endDate={item.endDate} id={item.id} owner={item.owner} />
                        </div>
                    )
                    :
                    <LoadingIcon />
                : <div>
                    <h1>Login To View the Blockchain</h1>
                </div>

            }
        </div>
    );


};


export default OpenElections;
