import React, { useRef, useState } from 'react';
import Axios from 'axios';
import Web3 from 'web3';
import { TiTick } from 'react-icons/ti';
import { MdClose } from 'react-icons/md';
import LoadingIcon from './LoadingIcon';


interface RegProps {
    address: string
    isValidPassword: Function
    contractAccount: any
}


const Registration = (props: RegProps) => {
    //Reference Getters
    const signUp: React.RefObject<HTMLInputElement> = useRef<HTMLInputElement>(null); //Gets Public Address
    const passwordOne: React.RefObject<HTMLInputElement> = useRef<HTMLInputElement>(null); //Get password input
    const passwordTwo: React.RefObject<HTMLInputElement> = useRef<HTMLInputElement>(null); //Get password confirmation input
    const securityQuestion: React.RefObject<HTMLSelectElement> = useRef<HTMLSelectElement>(null); //Get security question
    const securityAnswer: React.RefObject<HTMLInputElement> = useRef<HTMLInputElement>(null); //Get security answer


    //state
    const [errorPassword, setErrorPassword] = useState<string>("");
    const [errorPublic, setErrorPublic] = useState<string>("");
    const [securityError, setSecurityError] = useState<string>("");

    //loading
    const [loading, setLoading] = useState<boolean>(false);

    //password requirements
    const [hasUppercase, setHasUppercase] = useState<boolean>(false);
    const [hasLowercase, setHasLowercase] = useState<boolean>(false);
    const [hasSpecial, setHasSpecial] = useState<boolean>(false);
    const [hasDigit, setHasDigit] = useState<boolean>(false);
    const [eightLetters, setEightLetters] = useState<boolean>(false);

    //Function: handleSubmitRegistration @params: event: React.FormEvent<HTMLFormElement>
    //Purpose Validations registration Details then sends data to the server

    //button disabled
    const [buttonDisabled, setButtonDisabled] = useState<boolean>(false);


    const handleSubmitRegistration = async (event: React.FormEvent<HTMLFormElement>) => {
        setButtonDisabled(true);
        event.preventDefault();

        let error: boolean = false;

        if (!securityAnswer.current || securityAnswer.current.value.length == 0) {
            setSecurityError("Provide an answer");
            error = true;
        }



        //checks passwords match and exist
        if (passwordOne.current && passwordTwo.current) {
            if (passwordOne.current.value !== passwordTwo.current.value && !props.isValidPassword(passwordOne.current.value)) {
                error = true;
            }
            else if (passwordOne.current.value !== passwordTwo.current.value) {
                error = true;
                setErrorPassword("Provide Matching Valid Passwords")
            } else if (!props.isValidPassword(passwordOne.current.value)) {
                error = true;
            } else {
                setErrorPassword("");
            }

        }

        //check for errors
        if (!error && signUp.current && passwordOne.current && securityQuestion.current && securityAnswer.current) {

            let question = securityQuestion.current.value;
            let answer = securityAnswer.current.value;

            if (signUp.current) {

                let address = Web3.utils.isAddress(signUp.current.value) ? signUp.current.value : null

                if (address && props.contractAccount) {
                    setLoading(true);
                    props.contractAccount.methods.addNewAddress(Web3.utils.soliditySha3(passwordOne.current.value), question, Web3.utils.soliditySha3(answer)).send({ from: address })
                        .on('receipt', () => {
                            setLoading(false);
                            //update the web session
                            Axios.post('/registration', {
                                public_address: address
                            })
                                .then((result) => {
                                    if (result.data != "unsuccessful") {
                                        window.location.pathname = '/account';
                                        setButtonDisabled(false);
                                    } else {
                                        setButtonDisabled(false);
                                        setErrorPublic("Metamask submit successful, try to login in a minute");
                                    }
                                }).catch(() => {

                                    setButtonDisabled(false);
                                    setErrorPublic("Metamask submit successful, try to login in a minute");
                                })
                        }
                        )
                        .catch(() => {
                            setLoading(false);
                            setButtonDisabled(false);
                            setErrorPublic("Public Address already in use");
                        });

                } else {
                    setLoading(false);
                    setButtonDisabled(false);
                    setErrorPublic("Provide a Valid Public Address Via MetaMask");
                }
            }
        }else{
            setButtonDisabled(false);
        }
    }

    return (
        <div className={"columnsize-6 registration"}>
            <h3>Register An Account</h3>
            <form onSubmit={handleSubmitRegistration} action={"/"} >
                <h4 className={"margin-bottom-1 margin-top-0"}>Personal Details</h4>
                <div>
                    <h5 className={"margin-bottom-1 margin-top-0"}>Public Address (AutoFilled)</h5>
                    <input type="text" ref={signUp} className="inputBox" placeholder="Getting Public Address...." disabled value={props.address} />
                    {props.address == "0x" ?
                        <div className="error">
                            Check Metamask is connected and been given Privileges
                    </div>
                        : null}
                </div>
                {errorPublic ?
                    <div className="error">
                        {errorPublic}
                    </div> :
                    null}
                <h4 className={"margin-bottom-1 margin-top-0"} >Password</h4>
                <div>
                    <input type="password" ref={passwordOne} onChange={() => {
                        if (passwordOne.current) {
                            setHasLowercase(/[a-z]/g.test(passwordOne.current.value));
                            setHasSpecial(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g.test(passwordOne.current.value));
                            setHasUppercase(/[A-Z]/g.test(passwordOne.current.value));
                            setHasDigit(/[0-9]/g.test(passwordOne.current.value));
                            setEightLetters((passwordOne.current.value.length >= 8));
                        }

                    }} className="inputBox" placeholder="Your Password *" />
                </div>
                <div>
                    <input type="password" ref={passwordTwo} className="inputBox" placeholder="Confirm Your Password" />
                </div>
                <div className="requirements margin-top-0">
                    <p className={hasUppercase ? "valid" : "invalid"}>{hasUppercase ? <TiTick /> : <MdClose />} One Uppercase Character</p>
                    <p className={hasLowercase ? "valid" : "invalid"}>{hasLowercase ? <TiTick /> : <MdClose />}  One Lowercase Character</p>
                    <p className={hasSpecial ? "valid" : "invalid"}>{hasSpecial ? <TiTick /> : <MdClose />}  One Special Character</p>
                    <p className={hasDigit ? "valid" : "invalid"}>{hasDigit ? <TiTick /> : <MdClose />}  One Digit Character</p>
                    <p className={eightLetters ? "valid" : "invalid"}>{eightLetters ? <TiTick /> : <MdClose />}  At least 8 Character</p>
                </div>
                {errorPassword ?
                    <div className="error">
                        {errorPassword.split("\n").map((i, key) => {
                            return <div key={key}>{i}</div>;
                        })}
                    </div> :
                    null}
                <h4 className={"margin-bottom-1 margin-top-0"}>Security Question</h4>
                <p>This will be used to reset your password if you forget it</p>
                <select ref={securityQuestion}>
                    <option value="0">What was the name of your second pet?</option>
                    <option value="1">What was the last name of your third-grade teacher?</option>
                    <option value="2">Who was your childhood hero?</option>
                </select>
                <input ref={securityAnswer} type="text" className="inputBox" />
                {securityError ?
                    <div className="error">
                        {securityError}
                    </div> :
                    null}
                <div>
                    {
                        loading ?
                            <div>
                                <h3>Currently Processing Transaction</h3>
                                <LoadingIcon />
                            </div> :
                            null
                    }
                    <input disabled={buttonDisabled} type="submit" className="btnSubmit" />
                </div>
            </form>
        </div>
    )

};


export default Registration;
