import React from 'react';


interface INotFound {
}

const NotFound = (props: INotFound) => {
    
    return (
        <div className={"row"}>
            <div className={"columnsize-12"}>
                <h1 className="Text404">404 :(</h1>
                <h2 className="Message404">Page Could not be Found</h2>
            </div>
        </div>
    );

};


export default NotFound;
