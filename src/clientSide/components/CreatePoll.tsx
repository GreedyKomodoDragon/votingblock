import React, { useRef, useState } from 'react';
import Axios from 'axios';
import Web3 from 'web3';
import LoadingIcon from './LoadingIcon';

import { debounce } from "lodash";


interface CreatePollProps {
  account: string,
  convertHexToRGB: Function,
  convertRGBToHex: Function,
  contractMain: any
}

interface Candidate {
  id: number,
  name: string,
  party: string,
  red: number,
  green: number,
  blue: number
}

const CreatePoll = (props: CreatePollProps) => {
  //State
  const [candidateList, setCandidateList] = useState<Candidate[]>([]);
  const [electionName, setElectionName] = useState<string>("");
  const [description, setDescription] = useState<string>("");
  const [date, setDate] = useState<string>("");
  const [whitelist, setWhitelist] = useState<string[]>([]);
  const [whitelistOn, setWhitelistOn] = useState<boolean>(false);
  const [stage, setStage] = useState<number>(0);
  const [error, setError] = useState<string>("");
  const [deposit, setDeposit] = useState<string>("");
  const [reward, setReward] = useState<string>("");
  const [depositFiat, setDepositFiat] = useState<string>("");
  const [rewardFiat, setRewardFiat] = useState<string>("");
  const [currency, setCurrency] = useState<string>("USD");
  const [traditional, setTraditional] = useState<boolean>(true);

  //tickers
  const [USD2ETH, setUSD2ETH] = useState<number>(0)
  const [GBP2ETH, setGBP2ETH] = useState<number>(0)
  const [EUR2ETH, setEUR2ETH] = useState<number>(0)

  const [ETH2USD, setETH2USD] = useState<number>(0)
  const [ETH2GBP, setETH2GBP] = useState<number>(0)
  const [ETH2EUR, setETH2EUR] = useState<number>(0)

  //disable button
  const [createButtonDisabled, setCreateButtonDisabled] = useState<boolean>(false);

  //references
  const name = useRef<HTMLInputElement>(null);
  const white = useRef<HTMLInputElement>(null);
  const party = useRef<HTMLInputElement>(null);
  const colour = useRef<HTMLSelectElement>(null);
  const etherDeposit = useRef<HTMLInputElement>(null);

  //loading
  const [loading, setLoading] = useState<boolean>(false);

  const createCandidate = () => {

    let candidates: Candidate[] = candidateList;


    if (name.current && party.current && colour.current) {

      const [r, g, b] = props.convertHexToRGB(colour.current.value)

      const nameTrimmed = name.current.value.trim();
      const partyTrimmed = party.current.value.trim();

      if (nameTrimmed.length > 0 && partyTrimmed.length > 0) {
        candidates.push({ id: candidates.length, name: nameTrimmed, party: partyTrimmed, red: r, green: g, blue: b })
        setCandidateList([...candidates]);
        party.current.value = "";
        name.current.value = "";
      }

    }


  };

  const currentDate = () => {
    //getting the current date
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0');
    let yyyy = today.getFullYear();
    let StringDate: string = yyyy + '-' + mm + '-' + dd;

    return StringDate;
  };

  const exchange = (e: React.ChangeEvent<HTMLInputElement>, setEther: React.Dispatch<React.SetStateAction<string>>, setFiat: React.Dispatch<React.SetStateAction<string>>, fiat: string) => {
   
    if (typeof e.target.value != "undefined") {
      if (/^\s*-?\d+(\.\d{0,2})?\s*$/.test(e.target.value)) {
        let value = e.target.value;
        setFiat(value);

        if (currency == "USD") {

          if (!USD2ETH) {
            Axios.get('https://api.cryptonator.com/api/ticker/ETH-' + currency)
              .then((result) => {
                if (result.data) {
                  setEther((parseFloat(value) / parseFloat(result.data.ticker.price)) + "");
                  
                  setUSD2ETH(result.data.ticker.price);
                }
              }).catch(() => {
                setEther("0");
              });
          } else {
            setEther((parseFloat(value) / USD2ETH) + "");
          }

        } else if (currency == "GBP") {

          if (!GBP2ETH) {
            Axios.get('https://api.cryptonator.com/api/ticker/ETH-' + currency)
              .then((result) => {
                if (result.data) {
                  setEther((parseFloat(value) / parseFloat(result.data.ticker.price)) + "");
                  setGBP2ETH(result.data.ticker.price);
                }
              }).catch(() => {
                setEther("0");
              });
          } else {
            setEther((parseFloat(value) / GBP2ETH) + "");
          }

        } else {
          if (!EUR2ETH) {
            Axios.get('https://api.cryptonator.com/api/ticker/ETH-' + currency)
              .then((result) => {
                if (result.data) {
                  setEther((parseFloat(value) / parseFloat(result.data.ticker.price)) + "");
                  setEUR2ETH(result.data.ticker.price);
                }
              }).catch(() => {
                setEther("0");
              });
          } else {
            setEther((parseFloat(value) / EUR2ETH) + "");
          }
        }


      } else if (e.target.value.length == 0) {
        setEther("");
        setFiat("");
      }
    } else {
      setEther("");
    }

  };

  const addWhiteAddress = (address: string) => {
    if (Web3.utils.isAddress(address) && !whitelist.includes(address)) {
      let oldList = whitelist;

      oldList.push(address);

      setWhitelist([...oldList]);
    }
  }

  const isValidDateFormat = (date: string) => {
    const dateRegx: RegExp = /^\d{4}(-)(((0)[0-9])|((1)[0-2]))(-)([0-2][0-9]|(3)[0-1])$/;
    return dateRegx.test(date);
  };

  const handleChangeDate = (value: string) => {
    if (isValidDateFormat(value)) {
      setDate(value);
    }
  };

  const handleChangeName = (value: string) => {
    //TODO: Validation on value
    setElectionName(value);
  };

  const numberOfDays = (dayOne: string, dayTwo: string) => {
    const _MS_PER_DAY: number = 1000 * 60 * 60 * 24;

    const one: Date = new Date(dayOne);
    const two: Date = new Date(dayTwo);

    const utc1 = Date.UTC(one.getFullYear(), one.getMonth(), one.getDate());
    const utc2 = Date.UTC(two.getFullYear(), two.getMonth(), two.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
  }

  const createElection = async () => {

    setCreateButtonDisabled(true);

    let numberOfDaysLeft = numberOfDays(currentDate(), date);
    setError("");

    if (
      candidateList.length > 1 &&
      electionName && electionName.length < 32 &&
      props.account && props.account != "0x" &&
      date && isValidDateFormat(date) && numberOfDaysLeft > 0 &&
      description && description.length > 0 && description.length < 251 &&
      /^\d+(\.\d{0,18})?$/.test(reward) &&
      /^\d+(\.\d{0,18})?$/.test(deposit)
    ) {

      if (props.contractMain && !error) {
        setLoading(true);
        props.contractMain.methods.createElection(candidateList, Web3.utils.fromUtf8(electionName.trim()), traditional ? 0 : 1, numberOfDaysLeft, whitelist, Web3.utils.toWei(reward, "ether"), description).send({ from: props.account, value: Web3.utils.toWei(deposit, "ether") })
          .on('receipt', function () {

            setLoading(false);
            window.location.pathname = '/account';
            setCreateButtonDisabled(false);

          })
          .on('error', () => {
            setLoading(false);
            setError("Transaction was unable to submit election");
            setCreateButtonDisabled(false);
          });
      }

    } else {
      let errorMessage: string = "";

      if (currentDate() >= date) {
        errorMessage += "- Date selected cannot be used \n";
      }
      if (electionName.trim().length > 32) {
        errorMessage += "- Name of election must be less then 32 characters \n";
      }
      if (electionName.trim().length == 0) {
        errorMessage += "- Name of election not filled in \n";
      }
      if (description.length == 0) {
        errorMessage += "- Invalid Description \n";
      }

      if (candidateList.length <= 1) {
        errorMessage += "- Must have at least 2 candidates \n";
      }

      //check reward input
      if (!(/^\d+(\.\d{0,18})?$/.test(reward))) {
        errorMessage += "- Please set a reward, hence if 0 \n";
      }

      //check deposit
      if (!(/^\d+(\.\d{0,18})?$/.test(deposit))) {
        errorMessage += "- Please det a deposit, hence if 0 \n";
      }

      setError(errorMessage);
      setCreateButtonDisabled(false);
    }
  };

  const changeCurrency = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setCurrency(e.target.value);

    let newCurrency = e.target.value;

    //update fiats to new currency

    //check reward
    if (rewardFiat.length != 0) {
      if (newCurrency == "USD") {
        if (!ETH2USD) {
          Axios.get('https://api.cryptonator.com/api/ticker/' + newCurrency + "-ETH")
            .then((result) => {
              if (result.data) {
                setRewardFiat((parseFloat(reward) / parseFloat(result.data.ticker.price)) + "");
                setETH2USD(result.data.ticker.price);
              }
            }).catch(() => {
              setRewardFiat("0");
            });
        } else {
          setRewardFiat((parseFloat(reward) / USD2ETH) + "");
        }

      } else if (currency == "GBP") {

        if (!ETH2USD) {
          Axios.get('https://api.cryptonator.com/api/ticker/' + newCurrency + "-ETH")
            .then((result) => {
              if (result.data) {
                setRewardFiat((parseFloat(reward) / parseFloat(result.data.ticker.price)) + "");
                setETH2GBP(result.data.ticker.price);
              }
            }).catch(() => {
              setRewardFiat("0");
            });
        } else {
          setRewardFiat((parseFloat(reward) / ETH2GBP) + "");
        }

      } else {
        if (!ETH2USD) {
          Axios.get('https://api.cryptonator.com/api/ticker/' + newCurrency + "-ETH")
            .then((result) => {
              if (result.data) {
                setRewardFiat((parseFloat(reward) / parseFloat(result.data.ticker.price)) + "");
                setETH2EUR(result.data.ticker.price);
              }
            }).catch(() => {
              setRewardFiat("0");
            });
        } else {
          setRewardFiat((parseFloat(reward) / ETH2EUR) + "");
        }
      }
    }

    //check deposit
    if (deposit.length != 0) {
      if (newCurrency == "USD") {
        if (!ETH2USD) {
          Axios.get('https://api.cryptonator.com/api/ticker/' + newCurrency + "-ETH")
            .then((result) => {
              if (result.data) {
                setDepositFiat((parseFloat(deposit) / parseFloat(result.data.ticker.price)) + "");
                setETH2USD(result.data.ticker.price);
              }
            }).catch(() => {
              setDepositFiat("0");
            });
        } else {
          setDepositFiat((parseFloat(deposit) / USD2ETH) + "");
        }

      } else if (currency == "GBP") {

        if (!ETH2USD) {
          Axios.get('https://api.cryptonator.com/api/ticker/' + newCurrency + "-ETH")
            .then((result) => {
              if (result.data) {
                setDepositFiat((parseFloat(deposit) / parseFloat(result.data.ticker.price)) + "");
                setETH2GBP(result.data.ticker.price);
              }
            }).catch(() => {
              setDepositFiat("0");
            });
        } else {
          setDepositFiat((parseFloat(deposit) / ETH2GBP) + "");
        }

      } else {
        if (!ETH2USD) {
          Axios.get('https://api.cryptonator.com/api/ticker/' + newCurrency + "-ETH")
            .then((result) => {
              if (result.data) {
                setDepositFiat((parseFloat(deposit) / parseFloat(result.data.ticker.price)) + "");
                setETH2EUR(result.data.ticker.price);
              }
            }).catch(() => {
              setDepositFiat("0");
            });
        } else {
          setDepositFiat((parseFloat(deposit) / ETH2EUR) + "");
        }
      }
    }

  };

  const handleEther = (e: React.ChangeEvent<HTMLInputElement>, setEther: React.Dispatch<React.SetStateAction<string>>, setFiat: React.Dispatch<React.SetStateAction<string>>, ether: string) => {

    if (typeof e.target.value != "undefined") {

      if (/^\d+(\.\d{0,18})?$/.test(e.target.value)) {
        let value = e.target.value;
        setEther(value);

        Axios.get('https://api.cryptonator.com/api/ticker/' + currency + "-ETH")
          .then((result) => {
            if (result.data) {
              let amount = (parseFloat(value) / parseFloat(result.data.ticker.price));
              amount = Math.round((amount + Number.EPSILON) * 100) / 100
              setFiat(amount+ "");
            }
          }).catch(() => {
            setFiat("0");
          })
      } else if (e.target.value.length == 0) {
        setEther("");
        setFiat("");
      }
    } else {
      setFiat("0");
    }
  };

  const showCurrentProgress = (stage: number) => {
    switch (stage) {
      case 0:
        let nameValue = "";
        if (electionName) {
          nameValue = electionName;
        }

        let dateValue = "";
        if (date) {
          dateValue = date;
        }

        return (
          <div>
            <div className={"columnsize-12 topBox"}>
              <h2>Name and Duration</h2>
              <label>
                <span>Election Name:</span>
                <input className={"nameInput"} type="text" value={nameValue} onChange={e => handleChangeName(e.target.value)} placeholder="e.g. UK General Election" />
              </label>
              <label>
                <span>End Date:</span>
                <input type="date" value={dateValue} onChange={e => handleChangeDate(e.target.value)} />
              </label>
            </div>
            {
              electionName && electionName.trim().length > 32 ?
                <div className={"columnsize-12"}>
                  Name of the Election can be no longer than 32 counts, you are at: {electionName.length}
                </div>
                : null
            }
            <div className={"columnsize-12"}>
              <h2>Description</h2>
              <textarea maxLength={250} value={description} rows={3} className={"textarea-fill"} onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
                if (e.currentTarget && e.currentTarget.value.length < 251) {
                  setDescription(e.currentTarget.value)
                }
              }}>
              </textarea>
              <h5>Word Count: {description.length}</h5>
            </div>

            <div className={"columnsize-12"}>
              <h2>Election Type:</h2>
              <div className="toggle">
                <button onClick={() => {
                  setTraditional(true)
                }} className={traditional ? "button is-active" : "button"}>Single Vote</button>
                <button onClick={() => {
                  setTraditional(false)
                }} className={!traditional ? "button is-active" : "button"}>Ranking</button>
              </div>
              {traditional ?
                <div>
                  <h4>Single Vote</h4>
                  <p>Voters pick their favourite candidate.</p>
                  Points to consider:
                  <ul>
                    <li>If voters think their favourite has a low chance of winning, voters may pick a candidiate they like with a better chance of winning</li>
                    <li>Adding extra candidates increases the chances the most popular candidate will win</li>
                    <li>Encourages a strategic approach by voters</li>
                  </ul>
                </div>
                :
                <div>
                  <h4>Ranking</h4>
                  <p>Candidates ranked from highest to lowest. If there were 5 candidates then 5 would be the best score and 1 the worst. The counters add up all the points each candidate receives and the one with the most points is the winner.</p>
                  Points to consider:
                  <ul>
                    <li>The Ranking tends to elect broadly acceptable candidates, rather than those supported by the majority</li>
                    <li>Adding extra candidates increases the number of points available in the election</li>
                    <li>Encourages a strategic approach by parties to nominations</li>
                  </ul>
                </div>
              }
            </div>
          </div>

        );
      case 1:
        return (
          <div>
            <div className={"columnsize-12 greyBox"}>
              <h4>Create a new Candidate</h4>
              <div>
                <label>
                  <span>Candidate Name</span>
                  <input type="text" ref={name} placeholder="e.g. John Smith" />
                </label>
                <label>
                  <span>Candidate Party</span>
                  <input type="text" ref={party} placeholder="e.g. Blue Party" />
                </label>
                <label>
                  <span>Party Colour</span>
                  <select className={"aqua"} onChange={(e: React.ChangeEvent<HTMLSelectElement>) => { colour.current ? e.currentTarget.style.background = colour.current.value : null }} ref={colour}>
                    <option value="#00FFFF">Aqua</option>
                    <option value="#000000">Black</option>
                    <option value="#0000FF">Blue</option>
                    <option value="#008000">Green</option>
                    <option value="#800000">Maroon</option>
                    <option value="#FF00FF">Pink</option>
                    <option value="#800080">Purple</option>
                    <option value="#FF0000">Red</option>
                    <option value="#C0C0C0">Silver</option>
                    <option value="#FFFFFF">White</option>
                    <option value="#FFFF00">Yellow</option>
                  </select>
                </label>
              </div>

              <div className={"centre"}>
                <input type={"button"} onClick={createCandidate} value="Add Candidate" />
              </div>
            </div>
            <div className={"columnsize-12"}>
              <table className={"candidateTable"}>
                <thead>
                  <tr>
                    <th>Candidate Name</th>
                    <th>Party</th>
                    <th>Party Colour</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {candidateList.map((item, key) =>
                    <tr key={key}>
                      <td>{item.name}</td>
                      <td>{item.party}</td>
                      <td style={{ backgroundColor: props.convertRGBToHex(item.red, item.green, item.blue) }}></td>
                      <td><button className="removeButton" onClick={() => {
                        //get state
                        let candidateBefore: Candidate[] = candidateList;

                        //remove single value from the array
                        let candidateAfter = candidateBefore.filter((candidate) => {
                          return candidate.id !== item.id;
                        });

                        //update the candidate's ids
                        let id: number = 0;
                        candidateAfter.forEach(candidate => {
                          candidate.id = id;
                          id++;
                        });

                        //set state to new values
                        setCandidateList([...candidateAfter]);

                      }}>Remove</button></td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
          </div>);
      case 2:

        return (
          <div>
            <h2>Voters</h2>
            <div className={"columnsize-12"}>
              <h3>Ether Deposit</h3>
              <p>Each vote will cost the voter. It is up to you to whether you put a deposit down in order to cover the costs of the election.</p>
              <p>It is not required although it would encourage users to vote. Average Vote costs = 0.002201ETH, subject to change due to gas cost is adjusted on the blockchain according to the USD.</p>
              <select className="dropdownCurrency" defaultValue={"USD"} onChange={changeCurrency}>
                <option value="USD" >ETH to USD</option>
                <option value="GBP">ETH to GBP</option>
                <option value="EUR">ETH to EUR</option>
              </select>
              <label>
                <span>Ether: </span>
                <input className="etherInput" ref={etherDeposit} type="text" onChange={e => {
                  handleEther(e, setDeposit, setDepositFiat, deposit);
                }} value={deposit} placeholder="e.g. 1" />
              </label>
              <label>
                <span>{currency}: </span>
                <input className="etherInput" type="text" onChange={e => exchange(e, setDeposit, setDepositFiat, depositFiat)} value={depositFiat} placeholder="e.g. 1" />
              </label>
            </div>
            <div className={"columnsize-12"}>
              <h3>Rewarding a Voter</h3>
              <p>To Attach users you can set a reward in the form of ether to the user.</p>
              <label>
                <span>Ether Reward: </span>
                <input className="etherInput" type="text" onChange={e => handleEther(e, setReward, setRewardFiat, reward)} value={reward} placeholder="e.g. 1" />
              </label>
              <label>
                <span>{currency}: </span>
                <input className="etherInput" type="text" onChange={e => exchange(e, setReward, setRewardFiat, rewardFiat)} value={rewardFiat} placeholder="e.g. 1" />
              </label>
            </div>
            <h3>By Default Everyone is able to vote in your election</h3>

            <p>You can enable restrictive voting, the address you place into the table are the only addres that are allowed to vote in the election. </p>
            <p>Toggle the switch below to enable restrictive voting; adding no addresses will disable feature on submission</p>
            <label className="toggle-check">
              <input defaultChecked={whitelistOn} type="checkbox" className="toggle-check-input" />
              <span className="toggle-check-text" onClick={() => {
                if (whitelistOn) {
                  setWhitelist([]);
                  setWhitelistOn(false);
                } else {
                  setWhitelistOn(true);
                }
              }}></span>
            </label>
            {
              whitelistOn ?
                <div>
                  <div className={"columnsize-12 greyBox"}>
                    <h4>Adding a user to whitelist</h4>
                    <p>Adding addresses allows for only these addresses to be to vote</p>
                    <label className={"white"}>
                      <span>Address:</span>
                      <input type="text" ref={white} placeholder="e.g. 0xc46808a12fc70D374f66157290C38f63d98da970" />
                      <input className={"addressButton"} type={"button"} onClick={() => { if (white.current) { addWhiteAddress(white.current.value); white.current.value = ""; } }} value="Add Address" />
                    </label>
                    <p>Omittion of any address within the whitelist removes the whitelist feature</p>
                  </div>
                  <div className={"columnsize-12"}>
                    <table className={"whitelistTable"}>
                      <thead>
                        <tr>
                          <th>Address</th>
                        </tr>
                      </thead>
                      <tbody>
                        {whitelist.map((item, key) =>
                          <tr key={key}>
                            <td>{item}</td>
                          </tr>
                        )}
                      </tbody>
                    </table>
                  </div>
                </div> : null
            }
          </div>

        );
      case 3:
        return (
          <div className={"columnsize-12"}>
            <h2>Details</h2>
            <h3>Name: {electionName ? electionName : "Not Set"}</h3>
            <h3>End Date: {date ? date : "Not Set"}</h3>
            <h3>Voting System Selected: {traditional ? "Single Vote" : "Ranking"}</h3>
            <h2>Voters</h2>
            <p>Deposit: {/^\d+(\.\d{0,18})?$/.test(deposit) ? deposit : "Unknown"}</p>
            <p>Reward: {/^\d+(\.\d{0,18})?$/.test(reward) ? reward : "Unknown"}</p>
            {whitelistOn ?
              <div className={"columnsize-12"}>
                <table>
                  <thead>
                    <tr>
                      <th>Address</th>
                    </tr>
                  </thead>
                  <tbody>
                    {whitelist.map((item, key) =>
                      <tr key={key}>
                        <td>{item}</td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
              : <p>Everyone Can Vote</p>}
            <h2>Candidates</h2>
            <div className={"columnsize-12"}>
              <table>
                <thead>
                  <tr>
                    <th>Candidate Name</th>
                    <th>Party</th>
                    <th>Party Colour</th>
                  </tr>
                </thead>
                <tbody>
                  {candidateList.map((item, key) =>
                    <tr key={key}>
                      <td>{item.name}</td>
                      <td>{item.party}</td>
                      <td style={{ backgroundColor: props.convertRGBToHex(item.red, item.green, item.blue) }}></td>
                    </tr>
                  )}
                </tbody>


              </table>
            </div>
            {
              loading ?
                <div>
                  <h3>Currently Processing Transaction</h3>
                  <LoadingIcon />
                </div>
                : null
            }
            <br />
            {
              error ?
                <div className="error">
                  {error.split('\n').map((item, i) => {
                    return <p key={i}>{item}</p>;
                  })}
                </div>
                :
                null
            }
            <input type="button" className="createPoll" onClick={createElection} value="Create Election" disabled={createButtonDisabled} />
          </div>);

    }
  };

  const showProgress = () => {
    let classArray: string[] = [];
    switch (stage) {
      case 0:
        classArray = ["is-current", "", "", ""];
        break;
      case 1:
        classArray = ["is-complete", "is-current", "", ""];
        break;
      case 2:
        classArray = ["is-complete", "is-complete", "is-current", ""];
        break;
      case 3:
        classArray = ["is-complete", "is-complete", "is-complete", "is-current"];
        break;
    }

    return (
      <ol className="progress-indicator">
        <li onClick={() => { setStage(0) }} className={classArray[0]} >
          <span id="name">Name and Duration</span>
        </li>
        <li onClick={() => { setStage(1) }} className={classArray[1]} >
          <span id="candidates">Candidates</span>
        </li>
        <li onClick={() => { setStage(2) }} className={classArray[2]} >
          <span id="voters">Voters</span>
        </li>
        <li onClick={() => { setStage(3) }} className={classArray[3]} >
          <span id="submisson">Submission</span>
        </li>
      </ol>);
  }

  return (
    <div className={"row"}>

      <div className={"columnsize-12"}>
        <h1>Election Creation:</h1>
        {showProgress()}
      </div>
      <div className={"columnsize-2"}>
        {
          stage == 0 ?
            null
            :
            <i className="arrow left" onClick={() => {
              if (stage != 0) {
                setStage(stage - 1);
              }
            }} />
        }
      </div>
      <div className={"columnsize-8"}>
        {showCurrentProgress(stage)}
      </div>
      <div className={"columnsize-2"}>
        {
          stage == 3 ?
            null
            :
            <i className="arrow right" onClick={() => {
              if (stage != 3) {
                setStage(stage + 1);
              }

            }} />
        }

      </div>
    </div>
  )
}

export default CreatePoll;
