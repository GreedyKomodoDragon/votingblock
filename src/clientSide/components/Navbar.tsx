import React, { useEffect, useRef, useState } from 'react';
import Identicon from './Identicon';
import { Link } from "react-router-dom";


interface NavProps {
    account: string
}


const Navbar = (props: NavProps) => {
    //state
    const [checkStatus, setCheckStatus] = useState<boolean>(false);
    const [showDropdownContent, setShowDropdownContent] = useState<boolean>(false);

    //references
    const checkBox = useRef<HTMLInputElement>(null);

    return (
        <nav>
            {props.account !== "0x" ?

                <div className="dropdown">
                    <p onClick={() => setShowDropdownContent(!showDropdownContent)}><Identicon size={12} account={props.account} />  ACCOUNT</p>
                    {showDropdownContent ?
                        <div className="dropdown-links">
                            <Link className={"mobileLink"} to={"/createpoll"} >Create Election</Link>
                            <Link className={"mobileLink"} to={"/open"} >Latest Elections</Link>
                            <Link to={"/account"} >My Overview</Link>
                            <Link className={"deskopLink"} to={"/account/myelections"} >My Elections</Link>
                            <Link className={"deskopLink"} to={"/account/settings"} >My Settings</Link>
                            <a href="/logout">Log out</a>
                        </div>
                        : null
                    }
                </div>
                :
                null
            }
            <div className={"mainNav"}>
                <a href="/" className={"logo"}>Voting Block</a>
                <ul className="links">
                    {props.account !== "0x" ?
                        <div className={"mobileHidden"}>
                            <li>
                                <form action="/search" method="GET">
                                    <input name="search" type={"text"} placeholder="Search Elections..." />
                                </form>
                            </li>
                            <li><Link to={"/createpoll"}>CREATE ELECTION</Link></li>
                            <li><Link to={"/open"}>ELECTIONS RUNNING</Link></li>
                        </div>
                        :
                        <div>
                            <li><Link to={"/join"}>LOG IN</Link></li>
                        </div>
                    }

                </ul>
            </div>

        </nav>
    );


};


export default Navbar;
