import React, { useEffect, useState } from 'react';;
import { GiVote } from 'react-icons/gi';
import { FaArrowAltCircleRight } from 'react-icons/fa';
import Identicon from '../Identicon';
import Modal from './Modal';

interface IElectionItem {
    name: string,
    endDate: number,
    id: string,
    owner: string,
    account: string,
    description: string
}


const ElectionItem = (props: IElectionItem) => {

    const [modal, setModal] = useState<boolean>(false);

    useEffect(() => {

    });

    const isOver = () => {
        if (props.endDate < (new Date()).getTime() / 1000) {
            return "See Results";
        } else {
            return "Go Vote";
        }
    };

    const closeTab = () => {
        setModal(false);
    };


    return (
        <div className={"electionItem"}>
            <div className={"topBarItem"}>
                <GiVote /><span> {props.name}</span>
            </div>
            <div className={"contentItem"}>
                <p>Description: {props.description}</p>
                <div>
                    <div className={"tipSection"}>
                        <Identicon size={20} account={props.owner} /> <span>{props.owner}  {props.account != props.owner ? <button onClick={() => { setModal(true); }}>Send Tips</button> : null}</span>
                    </div>
                    {modal ? <Modal account={props.account} address={props.owner} close={closeTab} /> : null}
                </div>
                <div>
                    <button className="navigationButton" onClick={() => {
                        window.location.pathname = '/election/' + props.id;
                    }}>{isOver()}  <FaArrowAltCircleRight /></button>
                </div>
            </div>
        </div>
    );


};


export default ElectionItem;
