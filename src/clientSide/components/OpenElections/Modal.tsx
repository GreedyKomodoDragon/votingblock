import React, { useEffect } from 'react';
import Web3 from 'web3';

interface IModal {
    close: Function
    address: string,
    account: string
}


const Modal = (props: IModal) => {
    //state 

    const sendATip = (amount:string) =>{
        if(props.account != "0x"){
            const web3 = new Web3(Web3.givenProvider || "http://local:8545"); //connect to metaMask
            
            web3.eth.sendTransaction({to:props.address, from:props.account, value:Web3.utils.toWei(amount, "ether")})
            
        }
    };


    useEffect(() => {

    });

    return (
        <div className="modal">
            <div className="modal-content">
                <span onClick={() => {
                    props.close();
                }} className="close">&times;</span>
                <div>
                    <h4>Send a Tip to: {props.address}</h4>
                    <div className="row">
                        <div className="columnsize-6">
                            <button onClick={() =>{
                                sendATip("0.1");
                            }}>Send 0.1 Ether</button>
                        </div>
                        <div className="columnsize-6">
                            <button onClick={() =>{
                                sendATip("0.05");
                            }}>Send 0.05 Ether</button>
                        </div>
                        <div className="columnsize-6">
                            <button onClick={() =>{
                                sendATip("0.01");
                            }}>Send 0.01 Ether</button>
                        </div>
                        <div className="columnsize-6">
                            <button onClick={() =>{
                                sendATip("0.005");
                            }}>Send 0.005 Ether</button>
                        </div>
                        {
                            props.account == "0x" ?
                                <p>Must be signed in to tip a user</p>
                                : null
                        }
                    </div>
                </div>
            </div>

        </div>
    );


};

export default Modal;