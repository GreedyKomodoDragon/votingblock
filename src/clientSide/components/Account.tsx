import React from 'react';
import AccountMain from './Account/AccountMain';
import AccountSideBar from './Account/AccountSideBar';
import AccountElections from './Account/AccountElections';
import { Route } from 'react-router-dom';
import AccountSettings from './Account/AccountSettings';


interface AccountProps {
    account: string,
    convertTime(unix: number): string,
    convertRGBToHex: Function,
    contractMain: any,
    contractAccount: any
}


const Account = (props: AccountProps) => {

    return (
        <div>
            <div className={"columnsize-2"}>
                <AccountSideBar />
            </div>
            <div className={"content columnsize-10"}>
                <Route exact={true} path={'/account'} component={() => <AccountMain contractMain={props.contractMain} contractAccount={props.contractAccount} account={props.account} convertTime={props.convertTime} />} />
                <Route exact={true} path={'/account/myelections'} component={() => <AccountElections contractMain={props.contractMain} convertRGBToHex={props.convertRGBToHex} account={props.account} convertTime={props.convertTime}  />} />
                <Route exact={true} path={'/account/settings'} component={() => <AccountSettings contractAccount={props.contractAccount} account={props.account}/>} />
            </div>
        </div>
    )
};

export default Account;