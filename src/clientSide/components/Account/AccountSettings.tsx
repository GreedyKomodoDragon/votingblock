import React, { useState } from 'react';
import { TiTick } from 'react-icons/ti';
import { MdClose } from 'react-icons/md';
import Web3 from 'web3';
import LoadingIcon from '../LoadingIcon';


interface IAccountSettings {
    account: string,
    contractAccount: any
}


const AccountSettings = (props: IAccountSettings) => {

    //passwords 
    const [passwordOld, setPasswordOld] = useState<string>("");
    const [passwordNewOne, setPasswordNewOne] = useState<string>("");
    const [passwordNewTwo, setPasswordNewTwo] = useState<string>("");

    //security Question and answer
    const [answer, setAnswer] = useState<string>("");
    const [question, setQuestion] = useState<string>("");


    //loading
    const [loadingPassword, setLoadingPassword] = useState<boolean>(false);
    const [loadingQuestion, setLoadingQuestion] = useState<boolean>(false);

    //button disabled
    const [settingPasswordDisable, setSettingPasswordDisable] = useState<boolean>(false);
    const [settingQuestionDisable, setSettingQuestionDisable] = useState<boolean>(false);


    //errors 
    const [passwordError, setPasswordError] = useState<string>("");
    const [questionError, setQuestionError] = useState<string>("");

    //password requirements
    const [hasUppercase, setHasUppercase] = useState<boolean>(false);
    const [hasLowercase, setHasLowercase] = useState<boolean>(false);
    const [hasSpecial, setHasSpecial] = useState<boolean>(false);
    const [hasDigit, setHasDigit] = useState<boolean>(false);
    const [eightLetters, setEightLetters] = useState<boolean>(false);


    return (
        <div className={"row"}>
            <div className={"columnsize-12"}>
                <h2>Account Settings</h2>
                <h3>Update Password</h3>
            </div>
            <div className={"columnsize-6"}>
                <h5 className={"margin-bottom-1"} >Enter Old Password</h5>
                <input type="password" className={"inputBox-70 margin-top-0"} onChange={(e: React.ChangeEvent<HTMLInputElement>) => { setPasswordOld(e.currentTarget.value) }} />
                <h5 className={"margin-bottom-1"}>Enter New Password</h5>
                <input type="password" className={"inputBox-70 margin-top-0"} onChange={(e: React.ChangeEvent<HTMLInputElement>) => {

                    //update requirments
                    setHasLowercase(/[a-z]/g.test(e.currentTarget.value));
                    setHasSpecial(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g.test(e.currentTarget.value));
                    setHasUppercase(/[A-Z]/g.test(e.currentTarget.value));
                    setHasDigit(/[0-9]/g.test(e.currentTarget.value));
                    setEightLetters((e.currentTarget.value.length >= 8));

                    //update state
                    setPasswordNewOne(e.currentTarget.value);

                }} />
                <h5 className={"margin-bottom-1"}>Confirm New Password</h5>
                <input type="password" className={"inputBox-70 margin-top-0"} onChange={(e: React.ChangeEvent<HTMLInputElement>) => { setPasswordNewTwo(e.currentTarget.value) }} />

            </div>
            <div className={"columnsize-6"}>
                <h3>Password Requirements</h3>
                <div className="requirements enlargeRequirement">
                    <p className={hasUppercase ? "valid" : "invalid"}>{hasUppercase ? <TiTick /> : <MdClose />} One Uppercase Character</p>
                    <p className={hasLowercase ? "valid" : "invalid"}>{hasLowercase ? <TiTick /> : <MdClose />}  One Lowercase Character</p>
                    <p className={hasSpecial ? "valid" : "invalid"}>{hasSpecial ? <TiTick /> : <MdClose />}  One Special Character</p>
                    <p className={hasDigit ? "valid" : "invalid"}>{hasDigit ? <TiTick /> : <MdClose />}  One Digit Character</p>
                    <p className={eightLetters ? "valid" : "invalid"}>{eightLetters ? <TiTick /> : <MdClose />}  At least 8 Character</p>
                </div>
            </div>
            <div className={"columnsize-12"}>
                {
                    loadingPassword ?
                        <div>
                            <h3>Currently Processing Transaction</h3>
                            <LoadingIcon />
                        </div> :
                        null
                }
                <button className={"settingsButtonPassword"} disabled={settingPasswordDisable} onClick={async () => {
                    setSettingPasswordDisable(true);
                    
                    //check old password matches

                    if (hasDigit &&
                        hasLowercase &&
                        hasLowercase &&
                        hasSpecial &&
                        hasDigit &&
                        eightLetters &&
                        passwordNewOne === passwordNewTwo &&
                        props.contractAccount) {

                        let oldPassword = Web3.utils.soliditySha3(passwordOld);
                        setLoadingPassword(true);
                        props.contractAccount.methods.login(oldPassword).call({ from: props.account })
                            .then((result: any) => {
                                if (result && props.contractAccount) {
                                    let replacementPassword = Web3.utils.soliditySha3(passwordNewOne);
                                    props.contractAccount.methods.changePasswordWithOldPassword(oldPassword, replacementPassword).send({ from: props.account })
                                        .on('receipt', () => {
                                            setLoadingPassword(false);
    
                                            window.alert("Password Updated");
                                            setSettingPasswordDisable(false);
                                        }).catch(() => {
                                            setLoadingPassword(false);
                                            setPasswordError("Old Password Wrong");
                                            setSettingPasswordDisable(false);
                                        })
                                } else {
                                    setLoadingPassword(false);
                                    setSettingPasswordDisable(false);
                                    setPasswordError("Old Password Wrong")
                                }
                            })
                    } else {
                        setPasswordError("Check Passwords match and follow requirments");
                        setSettingPasswordDisable(false);
                    }
                    
                }} >Update Password</button>
                {
                    passwordError ?
                        <div className={"error"}>
                            {passwordError}
                        </div> :
                        null
                }
            </div>

            <div className={"columnsize-12"}>
                <h3>Update Security Question</h3>
                <select onChange={(e: React.ChangeEvent<HTMLSelectElement>) => {
                    setQuestion(e.currentTarget.value);
                }} className={"selectSettings"}>
                    <option value="0">What was the name of your second pet?</option>
                    <option value="1">What was the last name of your third-grade teacher?</option>
                    <option value="2">Who was your childhood hero?</option>
                </select>
                <input type="text" onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    setAnswer(e.currentTarget.value);
                }} className="margin-left-15 inputBox-70" />
            </div>
            <div className={"columnsize-12"}>
                {
                    loadingQuestion ?
                        <div>
                            <h3>Currently Processing Transaction</h3>
                            <LoadingIcon />
                        </div> :
                        null
                }
                <button className={"settingsButtonPassword"} disabled={settingQuestionDisable} onClick={() => {
                    setSettingQuestionDisable(true);
                    if (typeof question !== "undefined" && answer && props.contractAccount) {

                        let hashedAnswer = Web3.utils.soliditySha3(answer);
                        setLoadingQuestion(true);

                        props.contractAccount.methods.updateSecurityQuestion(question, hashedAnswer).send({ from: props.account })
                            .on('receipt', () => {
                                setLoadingQuestion(false);
                                setSettingQuestionDisable(false);
                                window.alert("sucessfully updated");

                            }).catch(() => {
                                setLoadingQuestion(false);
                                setSettingQuestionDisable(false);
                                setQuestionError("Unable to update the security question");
                            })

                    } else {
                        setSettingQuestionDisable(false);
                        setQuestionError("Answer can be no longer than 32 characters");
                    }

                }} >Update Security Question</button>
                {
                    questionError ?
                        <div className={"error"}>
                            {questionError}
                        </div> :
                        null
                }
            </div>
        </div>
    );


};


export default AccountSettings;
