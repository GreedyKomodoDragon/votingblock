import React, { useEffect, useState, } from 'react';
import Web3 from 'web3';
import LoadingIcon from '../LoadingIcon';
import DepositModal from './DepositModal';



interface IAccountElections {
    account: string,
    convertTime(unix: number): string,
    convertRGBToHex: Function,
    contractMain: any
}

const AccountElections = (props: IAccountElections) => {

    const [elections, setElections] = useState<any[]>();
    const [focused, setFocused] = useState<number>();
    const [modal, setModal] = useState<boolean>(false);

    const displayCandidates = (election: any[]) => {
        let candidates = [];

        for (let i = 0; i < election[6].length; i++) {
            candidates.push(
                <tr>
                    <td>{election[6][i]}</td>
                    <td>{election[7][i]}</td>
                    <td style={{ backgroundColor: props.convertRGBToHex(election[8][i], election[9][i], election[10][i]) }}></td>
                </tr>
            )
        }

        return candidates;
    };

    const closeTab = () => {
        setModal(false);

    };

    useEffect(() => {
        //Get last elections made by the user
        if (typeof elections == "undefined") {

            if (props.contractMain) {
                props.contractMain.methods.getOwnersElectionIDs().call({ from: props.account })
                    .then(async (electionsIDs: string | any[]) => {

                        let electionsFound = [];

                        for (let i = 0; i < electionsIDs.length; i++) {
                            if (props.contractMain) {
                                let found: any[] = await props.contractMain.methods.getElection(electionsIDs[i]).call({ from: props.account });
                                found[12] = electionsIDs[i];
                                electionsFound.push(found);
                            }
                        }

                        setElections(electionsFound);
                        setFocused(0);

                    })
            }


        }
    }, [modal]);

    return (
        <div className={"row"}>
            {
                typeof elections != "undefined" && elections.length > 0 && typeof focused != "undefined" ?
                    <div className={"row"}>
                        <div className={"columnsize-12"}>
                            <div className={"spacer10"}></div>
                        </div>
                        <div className={"columnsize-12"}>
                            <span className="selectElectionSpan">Election: </span><br/><select onChange={(e: React.ChangeEvent<HTMLSelectElement>) => { setFocused(parseInt(e.currentTarget.value)) }} className={"selectElection"}>
                                {elections ?
                                    elections.map((item, key) => {
                                        return (<option value={key} key={key}>{Web3.utils.toUtf8(item[1]).trim()}</option>);
                                    })
                                    : null
                                }

                            </select>
                            <hr></hr>
                            <h1>Election Details</h1>
                        </div>

                        <div className={"columnsize-12"}>
                            <h1>Election Candidates</h1>
                        </div>

                        <table>
                            <thead>
                                <tr>
                                    <th>Candidate Name</th>
                                    <th>Party</th>
                                    <th>Party Colour</th>
                                </tr>
                            </thead>
                            <tbody>
                                {displayCandidates(elections[focused])}
                            </tbody>
                        </table>
                        <div className={"columnsize-12"}>
                            <div className={"spacer10"}></div>
                        </div>

                        <div className={"columnsize-6"}>
                            <h3>Start Date: {props.convertTime(elections[focused][2])} </h3>
                            <h3>End Date: {props.convertTime(elections[focused][0])} </h3>
                            <h3>Ether Reward (ETH): {Web3.utils.fromWei(elections[focused][5], "ether")} </h3>
                        </div>

                        <div className={"columnsize-6"}>
                            <h3>Election Type: {elections[focused][11] == 0 ? "Single Vote" : "Borda"} </h3>
                            <h3>Deposit Left (ETH): {Web3.utils.fromWei(elections[focused][4], "ether")} </h3>
                            <button onClick={() => {
                                setModal(true);
                            }} className={"depositButton"}>Add Ether to Deposit</button>
                            {
                                modal ?
                                    <DepositModal contractMain={props.contractMain} close={closeTab} account={props.account} electionId={elections[focused][12]} />
                                    :
                                    null
                            }
                        </div>

                        
                    </div>
                    :
                    <div className={"columnsize-12"}>
                        <h2>Searching for your elections....</h2>
                        <LoadingIcon />
                        <h3>If you have not created any elections. Create an election and then come back to this page.</h3>
                    </div>
            }
        </div >
    );


};


export default AccountElections;
