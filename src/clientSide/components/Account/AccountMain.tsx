import React, { useEffect, useRef, useState, useLayoutEffect } from 'react';;
import { FaEthereum } from 'react-icons/fa';
import Identicon from '../Identicon';
import Web3 from 'web3';
import { MdAccountBox } from 'react-icons/md';
import LoadingIcon from '../LoadingIcon';
import { Vote, Election, white } from '../../exports/interfaces';



interface IAccountMain {
    account: string,
    convertTime(unix: number): string,
    contractMain: any,
    contractAccount: any
}


const AccountMain = (props: IAccountMain) => {

    const toggleContent = (tabNumber: number) => {
        switch (tabNumber) {
            case 0:
                return (
                    <div className={"columnsize-12"}>
                        <table className={"smallTable"}>
                            <thead>
                                <tr>
                                    <th>Election Name</th>
                                    <th className={"desktopOnly"}>Start Date</th>
                                    <th className={"desktopOnly"}>End Date</th>
                                    <th>Number Of Votes</th>
                                </tr>
                            </thead>
                            <tbody>
                                {elections ?
                                    elections.map((item, key) => {
                                        return (<tr className="left_td" key={key}>
                                            <td onClick={() => {
                                                window.location.pathname = "/election/" + item.id;
                                            }}>
                                                {Web3.utils.toUtf8(item.name)}
                                            </td>
                                            <td className={"desktopOnly"}>
                                                {props.convertTime(parseInt(item.startDate))}
                                            </td>
                                            <td className={"desktopOnly"}>
                                                {props.convertTime(parseInt(item.endDate))}
                                            </td>
                                            <td>
                                                {item.numberOfVotes}
                                            </td>
                                        </tr>);
                                    })
                                    : null
                                }

                            </tbody>
                        </table>
                        {elections ?
                            null
                            :
                            <LoadingIcon />
                        }
                    </div>);
            case 1:
                return (
                    <div className={"columnsize-12"}>
                        <table className={"smallTable"}>
                            <thead>
                                <tr>
                                    <th>Election Name</th>
                                    <th className={"desktopOnly"} >Date Of Vote</th>
                                    <th className={"desktopOnly"} >Vote Hex</th>
                                    <th className={"desktopOnly"}>Change vote?</th>
                                    <th>Verify Vote</th>
                                </tr>
                            </thead>
                            <tbody>
                                {votes ?
                                    votes.map((item, key) => {
                                        return (<tr key={key}>
                                            <td className="left_td" onClick={() => {
                                                window.location.pathname = '/election/' + item.electionID;
                                            }}>
                                                {Web3.utils.toUtf8(item.name)}
                                            </td>
                                            <td className={"desktopOnly"}>
                                                {props.convertTime(item.time)}
                                            </td>
                                            <td className={"desktopOnly breakItem"}>
                                                {item.hexValue}
                                            </td>
                                            <td className={"desktopOnly"}>
                                                {item.change ? "Yes" : "No"}
                                            </td>
                                            <td>
                                                <button onClick={async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
                                                    let hex: string = item.hexValue;
                                                    if (props.contractMain && props.account) {
                                                        let valid = await props.contractMain.methods.verifyVote(item.electionID, hex).call({ from: props.account });
                                                        if (valid) {
                                                            window.alert("Your vote is still valid");
                                                        } else {
                                                            window.alert("Vote seems to be tempered with, recommend re-voting");
                                                        }
                                                    }



                                                }} className={"verification"}>Verify</button>
                                            </td>
                                        </tr>);
                                    })
                                    : null
                                }

                            </tbody>
                        </table>
                        {votes ?
                            null
                            :
                            <LoadingIcon />
                        }
                    </div>);
            case 2:
                return (
                    <table className={"smallTable"}>
                        <thead>
                            <tr>
                                <th>Election Name</th>
                                <th>Owner</th>
                            </tr>
                        </thead>
                        <tbody>
                            {whitelist ?
                                whitelist.map((item, key) => {
                                    return (<tr onClick={() => {
                                        window.location.pathname = '/election/' + item.id;
                                    }} key={key}>
                                        <td>
                                            {Web3.utils.toAscii(item.electionName)}
                                        </td>
                                        <td className={"breakItem"}>
                                            {item.owner}
                                        </td>
                                    </tr>);
                                })
                                : null
                            }

                        </tbody>
                    </table>
                );
        }

    };

    //references
    const box: React.RefObject<HTMLDivElement> = useRef<HTMLDivElement>(null);

    //state
    const [width, setWidth] = useState<number>(0);
    const [id, setId] = useState<number>(0);
    const [foundRefund, setFoundRefund] = useState<boolean>();
    const [refund, setRefund] = useState<number>();
    const [votes, setVotes] = useState<Vote[]>();
    const [elections, setElections] = useState<Election[]>();
    const [whitelist, setWhitelist] = useState<white[]>();


    const [loading, setLoading] = useState<boolean>(false);

    useEffect(() => {

        //Get user votes
        if (!votes && props.contractMain) {
            props.contractMain.methods.getOwnVotes().call({ from: props.account })
                .then((result:any[]) => {
                    let [votesFound, numberOfVotesFound] = result;
                    let votesTogether: Vote[] = [];

                    if (votes) {
                        votesTogether = votesTogether.concat(votes, votesFound.slice(0, numberOfVotesFound));
                    } else {
                        votesTogether = votesFound.slice(0, numberOfVotesFound);
                    }

                    //set the election state
                    setVotes(votesTogether);

                });
        }

        if (!elections) {
            //Get elections made by the user
            if (props.contractMain) {
                props.contractMain.methods.getOwnersElectionIDs().call({ from: props.account })
                    .then((ids: any[]) => {

                        let electionsFound: Election[] = [];
                        if (ids.length > 0) {
                            ids.forEach(async (id: any) => {
                                if (props.contractMain) {
                                    let numberOfVotesFound = await props.contractMain.methods.getNumberOfVotes(id).call({ from: props.account });
                                    let electionFoundSingle = await props.contractMain.methods.getElection(id).call({ from: props.account });
                                    electionsFound.push({ name: electionFoundSingle[1], startDate: electionFoundSingle[2], endDate: electionFoundSingle[0], numberOfVotes: numberOfVotesFound, id: id });
                                    setElections([...electionsFound]);
                                }
                            })
                        } else {
                            setElections([...electionsFound]);
                        }


                    });
            }

        }

        //get whitelist
        if (!whitelist && props.contractMain && props.contractAccount) {
            props.contractAccount.methods.getwhitelist().call({ from: props.account })
                .then((ids: any[]) => {
                    let invitedList: white[] = [];

                    for (let i = 0; i < ids.length; i++) {
                        if (props.contractMain) {
                            props.contractMain.methods.getElection(ids[i]).call({ from: props.account })
                                .then((election: any) => {
                                    invitedList.push({ electionName: election[1], owner: election[3], id: ids[i] })
                                }).catch(() => {
                                    //do nothing
                                    console.log("Could not get ids");
                                })
                        }
                    }

                    setWhitelist(invitedList);
                });
        }

        //checking for refund
        if (!foundRefund && props.contractAccount) {
            props.contractAccount.methods.checkRefundAmount().call({ from: props.account })
                .then((refundAmount: number) => {
                    setRefund(refundAmount);
                    setFoundRefund(true);
                }).catch(() => {
                    console.log("refund could not be found")
                })
        }

    }, [])

    useLayoutEffect(() => {
        const updateWidth = () => {
            if (box.current) {
                setWidth(box.current.offsetWidth);
            }
        };

        window.addEventListener('resize', updateWidth);
        updateWidth();

        return () => window.removeEventListener('resize', updateWidth);

    }, []);

    return (
        <div className={"row"}>
            <div className={"columnsize-4"}>
                <div className="boxAccount">
                    <MdAccountBox />
                    <div>
                        <h2>Account:</h2>
                        <h3>{props.account}</h3>
                    </div>
                </div>
            </div>
            <div className={"columnsize-4"}>
                <div className="boxAccount">
                    <FaEthereum />
                    <div>
                        <h2>Refund Amount: </h2>
                        {foundRefund ?
                            <div>
                                {
                                    loading ?
                                        <div>
                                            <h3>Currently Processing Transaction</h3>
                                            <LoadingIcon />
                                        </div> :
                                        <h3 className={"ethValue"}>{Web3.utils.fromWei(refund + "", 'ether')} ETH</h3>
                                }
                                {refund != 0 ?
                                    <button onClick={() => {
                                        if (props.contractAccount) {
                                            setLoading(true);
                                            props.contractMain.methods.getRefund().send({ from: props.account })
                                                .on('receipt', () => {
                                                    setRefund(0);
                                                    setLoading(false);
                                                })
                                                .on('error', (e: any) => {
                                                    setLoading(false)
                                                    console.log(e);
                                                    window.alert("unable to get your refund")
                                                });
                                        }



                                    }}>Get Refund</button>
                                    :
                                    null
                                }
                            </div>
                            :
                            <LoadingIcon />
                        }
                    </div>

                </div>
            </div>
            <div className={"columnsize-4"}>
                <div className="boxAccount">
                    <h3>Your Identicon</h3>
                    <div ref={box} className={"centre"}>
                        <Identicon size={width} account={props.account} />
                    </div>
                </div>
            </div>
            <div className={"columnsize-12"}>
                <div className={"spacer10"}></div>
            </div>
            <div className={"columnsize-12"}>
                <h4>Outline: </h4>
            </div>
            <div className={"columnsize-12 tab"}>
                <select onChange={(e) =>{
                    setId(parseInt(e.currentTarget.value));
                }} >
                    <option value={0}>My Elections</option>
                    <option value={1}>My Votes</option>
                    <option value={2}>Election Invitations</option>
                </select>
            </div>
            {toggleContent(id)}
        </div>
    );


};


export default AccountMain;
