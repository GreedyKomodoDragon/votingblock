import React, { useEffect } from 'react';
import Web3 from 'web3';
import { useState } from 'react';
import LoadingIcon from '../LoadingIcon';


interface IDepositModal {
    close: Function
    electionId: string,
    account: string,
    contractMain: any
}


const DepositModal = (props: IDepositModal) => {
    //state 
    const [reason, setReason] = useState<string>("");
    const [amount, setAmount] = useState<string>("");


    //loading
    const [loading, setLoading] = useState<boolean>(false);

    useEffect(() => {

    }, []);

    return (
        <div className="modal">
            <div className="modal-content">
                <span onClick={() => { props.close(); }} className="close">&times;</span>
                <div>
                    <h4>Add Deposit</h4>
                    <div className="row">
                        <div className="columnsize-12">
                            {reason ?
                                <div className="error">
                                    Unable to Deposit Ether: {reason}
                                </div>
                                : null
                            }
                        </div>
                        <div className="columnsize-6">
                            <input className={"modalInput"} type="text" onChange={(e: React.ChangeEvent<HTMLInputElement>) => { setAmount(e.currentTarget.value) }} />
                        </div>
                        <div className="columnsize-6">
                            <button onClick={() => {
                                if (/^\d+(\.\d{0,18})?$/.test(amount)) {
                                    setLoading(true);
                                    if(props.contractMain){
                                        props.contractMain.methods.donateToElection(parseInt(props.electionId)).send({ from: props.account, value: Web3.utils.toWei(amount, "ether") })
                                            .on('receipt', () => {
                                                setLoading(false);
                                                props.close();
                                            }).catch(() => {
                                                setLoading(false);
                                                setReason("Transaction was rejected")
                                            })
                                    }else{
                                        setLoading(false);
                                        setReason("Contract could not be reached");
                                    }
                                } else {
                                    setReason("Not Valid Ether Value. A postive number to max 18dp")
                                }
                            }}>Add Deposit</button>

                        </div>
                        <div className="columnsize-12">
                            {
                                loading ?
                                    <div>
                                        <h3>Currently Processing Transaction</h3>
                                        <LoadingIcon />
                                    </div>
                                    : null
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

};

export default DepositModal;