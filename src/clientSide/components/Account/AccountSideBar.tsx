import React from 'react';
import { MdAccountBox } from 'react-icons/md';
import { IoMdInformationCircle, IoMdSettings } from 'react-icons/io'
import { Link } from 'react-router-dom';


interface IAccountSideBar {

}


const AccountSideBar = (props: IAccountSideBar) => {



    return (
        <div className={"sideBar floatLeft"}>
            <h3>Your Account</h3>
            <ul>
                <li>
                    <Link to={"/account"}><MdAccountBox/><span>My Overview</span></Link>
                </li>
                <li>
                    <Link to={"/account/myelections"}><IoMdInformationCircle/><span>My Elections</span></Link>
                </li>
                <li>
                    <Link to={"/account/settings"}><IoMdSettings/><span>My Settings</span></Link>
                </li>
            </ul>
        </div>
    );


};


export default AccountSideBar;
