import React from 'react';
import ReactPlayer from 'react-player'

interface IInstructions {
}

const Instructions = (props: IInstructions) => {


    const updateCss = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {

        if (e.currentTarget.className === "faq-content") {
            e.currentTarget.className = "faq-content panelOpen";
        } else {
            e.currentTarget.className = "faq-content";
        }
    }

    return (
        <div className={"row"}>
            <div className={"columnsize-12"}>
                <div className={"help"}>
                    <h1>Setting Up</h1>
                    <h4>Before you can use this website you need: </h4>
                    <ul>
                        <li>A Ethereum Wallet Built into Browser</li>
                        <li>Select the Ropsten Network (This is currently only on a test network)</li>
                        <li>Some Ether in your account</li>
                    </ul>
                    <h4>Our Recommendation would be to follow this tutorial: </h4>
                    <ReactPlayer width={"90vw"} url='https://www.youtube.com/watch?v=Sc8J98m6SZE' />
                    <h1>FAQs</h1>
                    <h2>Understanding the Concepts</h2>
                    <div onClick={(e) => {
                        updateCss(e)
                    }} className={"faq"}>
                        <div className="faq-question">
                            <div className="plus">+</div>
                            <label className="panel-title">What is a decentralised application?</label>
                            <div className="panel-content">A decentralised application (dApp) is an application that runs on a decentralised network and uses its resources</div>

                        </div>
                    </div>
                    <div onClick={(e) => {
                        updateCss(e)
                    }} className={"faq"}>
                        <div className="faq-question">
                            <div className="plus">+</div>
                            <label className="panel-title">What is the blockchain?</label>
                            <div className="panel-content">A blockchain is a time-stamped series of immutable records of data that is managed by a cluster of computers; not owned by any single entity or user. Each of these blocks of data is secured and bound to each other using cryptographic principles.</div>

                        </div>
                    </div>
                    <div onClick={(e) => {
                        updateCss(e)
                    }} className={"faq"}>
                        <div className="faq-question">
                            <div className="plus">+</div>
                            <label className="panel-title">What is a wallet?</label>
                            <div className="panel-content">Blockchain wallet is a computer program that allows to monitor and conduct cryptocurrency. Wallets transactions are recorded on the blockchain.</div>

                        </div>
                    </div>
                    <h2>Your Wallet and the website</h2>
                    <div onClick={(e) => {
                        updateCss(e)
                    }} className={"faq"}>
                        <div className="faq-question">
                            <div className="plus">+</div>
                            <label className="panel-title">How does my wallet interact with the website?</label>
                            <div className="panel-content">
                                The website will at times ask for you to provide Ethereum in order to submit or change any data. This cost is called "Gas" and is required to make any changes to the blockchain, where your data is stored.
                            <br />
                                <br />
                                You will be presented with a message box everytime a action that cost Ethereum arises, you can reject this transactions if you change your mind. Recommend following the tutorial above to get a greater understanding of how the wallet interacts with the website.
                        </div>

                        </div>
                    </div>
                    <div onClick={(e) => {
                        updateCss(e)
                    }} className={"faq"}>
                        <div className="faq-question">
                            <div className="plus">+</div>
                            <label className="panel-title">What browsers are supported by default?</label>
                            <div className="panel-content">By Default, most browsers are not supported with Ethereum Wallets. <a href="https://www.opera.com/crypto">Opera</a> and <a href="https://brave.com/">Brave</a> are the only browsers we know that supported with Ethereum Wallets by default, but we would only recommend you use Brave in this case.</div>
                        </div>
                    </div>
                    <div onClick={(e) => {
                        updateCss(e)
                    }} className={"faq"}>
                        <div className="faq-question">
                            <div className="plus">+</div>
                            <label className="panel-title">What do I do if my browser does not have a built in wallet?</label>
                            <div className="panel-content">MetaMask is the recommendation here, follow the toturial above and you should be all good to go. We even would go as far to say that we recommend Metamask even if your browser has one by default.</div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

};


export default Instructions;
