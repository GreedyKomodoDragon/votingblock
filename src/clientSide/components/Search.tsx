import React, { useEffect, useState } from 'react';
import LoadingIcon from './LoadingIcon';
import ElectionItem from './OpenElections/ElectionItem';
import { searchElection } from '../exports/interfaces';
import Web3 from 'web3';



interface SearchProps {
    convertTime(unix: number): string,
    account: string,
    contractMain: any
}


const Search = (props: SearchProps) => {
    //state
    const [elections, setElections] = useState<searchElection[]>();
    const [numberOfElections, setNumberOfElections] = useState<number>(0);
    const [param, setParams] = useState<String | null>("");

    useEffect(() => {
        //To get user details
        if (!elections && props.account != "0x") {
            let params: URLSearchParams = new URLSearchParams(window.location.search);
            let searchParam = params.get('search');
            if (searchParam != null && searchParam.trim().length > 0) {

                let keyword = params.get('search');
                setParams(keyword);

                if (keyword && keyword.length < 32 && props.contractMain) {


                    props.contractMain.methods.getElectionBasedOnName(keyword).call({ from: props.account })
                        .then(async (result: [any, any]) => {
                            let [electionsFound, numberOfElectionsFound] = result;

                            //for when you want to find more then 10
                            let electionsTogether: searchElection[] = [];
                            setElections([]);
                            if (electionsFound != 0) {
                                for (let i = 0; i < numberOfElectionsFound; i++) {
                                    if (props.contractMain) {
                                        let focusedElection: any[] = electionsFound[i];
                                        let description = await props.contractMain.methods.getElectionDescription(focusedElection[3]).call({ from: props.account });
                                        electionsTogether.push({ id: focusedElection[3], name: focusedElection[0], numberOfVotes: focusedElection[2], endDate: focusedElection[4], owner: focusedElection[1], description: description })
                                        setElections([...electionsTogether]);
                                    }
                                }
                                setNumberOfElections(numberOfElectionsFound);
                            } else {

                            }


                        })
                }
            } else {
                window.location.pathname = "/open";
            }
        }
    }, [props.account, elections, props.contractMain]);

    return (
        <div className={"row"}>
            <div className={"columnsize-12"}>
                <h1>Search Results</h1>
            </div>
            {
                props.account != "0x" ?
                    elections ?
                        elections.length > 0 ?
                            elections.map((item, key) => {
                                ;
                                return (
                                    <div className={"columnsize-6"}>
                                        <ElectionItem endDate={item.endDate} description={item.description} name={Web3.utils.toAscii(item.name)} id={item.id} owner={item.owner} account={props.account} />
                                    </div>
                                );
                            })
                            :
                            <div className={"columnsize-12"}>
                                <div>
                                    <h3 className="centre">Appears to be no results that match your search "{param}"</h3>
                                    <img className="noResultsIcon" src={"/images/no_results.png"} />
                                </div>
                            </div>
                        :
                        <div className={"columnsize-12"}>
                            {
                                param && param.length > 32 ?
                                    <div>
                                        <h3 className="centre">Search is limited to 32 character, try a lower character search</h3>
                                    </div>
                                    :
                                   <LoadingIcon />
                            }
                        </div>
                    :
                    <div>
                        <h1>Login To View the Blockchain</h1>
                    </div>
            }
        </div >

    );


};


export default Search;
