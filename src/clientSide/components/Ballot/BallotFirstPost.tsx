import React, { useEffect, useRef, useState } from 'react';
import { ElectionWithCandidates, candidate } from '../../exports/interfaces';

interface BallotFirstPostProps {
    setPrimativeVotes: Function,
    election: ElectionWithCandidates
}

const BallotFirstPost = (props: BallotFirstPostProps) => {

    //state
    const [selected, setSelected] = useState<number>();

    const handleVote = (selectedCan: number) =>{
        //create new array with candidate using index to set to 1
        let votes = new Array(props.election.candidates.length).fill(0);
        votes[selectedCan] = 1;

        //update states
        props.setPrimativeVotes(votes);
        setSelected(selectedCan);
    };
   
    return (
        <tbody>
            {
                props.election.candidates.map((value: candidate, index: number) => {
                    return (
                      <tr key={index}>
                        <td>
                          <div className={"colouredBox"} style={{ backgroundColor: value.colour }}></div>
                          <div className={"party"}>
                            <h1>{value.party}</h1>
                            <h2>Candidate: {value.name}</h2>
                          </div>
                        </td>
                        {selected === value.id ?
                          <td onClick={() => handleVote(value.id)} className={"box"}>&#9745;</td>
                          : <td onClick={() => handleVote(value.id)} className={"box"}>&#9744;</td>
                        }
                      </tr>
                    );
                  })
            }
        </tbody>
    );
};

export default BallotFirstPost;