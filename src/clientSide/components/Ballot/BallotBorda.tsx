import React, { useEffect } from 'react';
import { ElectionWithCandidates, candidate } from '../../exports/interfaces';

interface BallotBordaProps {
    setPrimativeVotes: Function,
    election: ElectionWithCandidates,
    primativeVotes: number[]
}

const BallotBorda = (props: BallotBordaProps) => {

    
    
    useEffect(()=>{
        //sets the default to have all candidates with lowest rank
        props.setPrimativeVotes(new Array(props.election.candidates.length).fill(1));
    },[])

    return (
        <tbody>
            {
                props.election.candidates.map((value: candidate, index: number) => {
                    return (
                        <tr key={index}>
                            <td>
                                <div className={"colouredBox"} style={{ backgroundColor: value.colour }}></div>
                                <div className={"party"}>
                                    <h1>{value.party}</h1>
                                    <h2>Candidate: {value.name}</h2>
                                </div>
                            </td>

                            <td>
                                <select className={"bordaBoxes"} onChange={(e: React.ChangeEvent<HTMLSelectElement>) =>{
                                    let prim = props.primativeVotes;
                                    prim[index] = parseInt(e.currentTarget.value);
                                    props.setPrimativeVotes(prim);
                                }}>
                                    {
                                        props.election.candidates.map((value: candidate, index: number) => {
                                            return (
                                                <option>{index + 1}</option>
                                            );
                                        })
                                    }
                                </select>
                            </td>

                        </tr>
                    );
                })
            }
        </tbody>
    );
};

export default BallotBorda;