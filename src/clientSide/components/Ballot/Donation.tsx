import React, { useRef, useState } from 'react';
const config = require('../../exports/contractConfig.js');
import Web3 from 'web3';
import Axios from 'axios';
import LoadingIcon from '../LoadingIcon';



interface DonationProps {
    electionID: string,
    account: string,
    contractMain: any
}

const Donation = (props: DonationProps) => {

    //state
    const [error, setError] = useState<string>("");

    //loading
    const [loading, setLoading] = useState<boolean>(false);

    //references
    const etherReward = useRef<HTMLInputElement>(null);

    return (
        <div className={"row topBottomSpace-50"}>
            <div className={"columnsize-12"}>
                <h2>Donation Towards this Election</h2>
            </div>
            <div className={"columnsize-6"}>

                <h4>In order to keep this election free for users, you may donation Ethereum to this election. Donate as much or as little as you want</h4>
                <label className={"labelCover"}>
                    <span>Ether Donation: </span>
                    <input ref={etherReward} onChange={(e: React.ChangeEvent<HTMLInputElement>) => { if (!(/^\d+(\.\d{0,18})?$/.test(e.target.value))) { setError('Ether Value is not valid'); } else { setError(''); } }} type="textbox" placeholder="E.g. 0.05" />
                    <button onClick={async () => {
                        if (props.account != "0x" && etherReward.current) {
                            if (/^\d+(\.\d{0,18})?$/.test(etherReward.current.value) && props.contractMain) {

                                //do some check on the donation ID
                                setLoading(true);
                                props.contractMain.methods.donateToElection(props.electionID).send({ from: props.account, value: Web3.utils.toWei(etherReward.current.value, "ether") })
                                    .on('receipt', () => {
                                        setLoading(false);
                                        window.alert("Sucessful Transaction");
                                        if (etherReward.current) {
                                            etherReward.current.value = "";
                                        }
                                    }).catch(() => {
                                        setLoading(false);
                                        setError('Transaction did not work, check Ethereum amount in wallet');
                                    })

                            } else {
                                setError('Ether Value is not valid');
                            }
                        } else {
                            setError('Cannot connect to wallet');
                        }


                    }}>Donate</button>
                </label>
                {error ?
                    <div className="row error">
                        {error}
                    </div>
                    :
                    null
                }
                {
                    loading ?
                        <div>
                            <h3>Currently Processing Transaction</h3>
                            <LoadingIcon />
                        </div> :
                        null
                }
            </div>

            <div className={"columnsize-6"}>
                <h4>What will happen to my donation if it is not used up?</h4>
                <p>All donations are tracked and what is left of your donation will be refunded</p>
                <h4>How do we use donations?</h4>
                <p>Donations are used up in the order they were donated in, if you donate before someone else you donation will be used up before theirs will be</p>
            </div>

        </div>
    );
};

export default Donation;