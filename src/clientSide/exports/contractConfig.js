module.exports = {
    ADDRESS_Homo: "0x7D0424A6fEE7ef9c837E74F1214F03113E25C406",
    ADDRESS_account: "0x43426e8C601dB829709759acfac72767b722cd39",
    ABIHomo :[
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "_accounts",
            "type": "address"
          }
        ],
        "stateMutability": "nonpayable",
        "type": "constructor"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_electionID",
            "type": "uint256"
          },
          {
            "internalType": "string[]",
            "name": "_vote",
            "type": "string[]"
          }
        ],
        "name": "addVote",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function",
        "payable": true
      },
      {
        "inputs": [
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "id",
                "type": "uint256"
              },
              {
                "internalType": "string",
                "name": "name",
                "type": "string"
              },
              {
                "internalType": "string",
                "name": "party",
                "type": "string"
              },
              {
                "internalType": "uint8",
                "name": "red",
                "type": "uint8"
              },
              {
                "internalType": "uint8",
                "name": "green",
                "type": "uint8"
              },
              {
                "internalType": "uint8",
                "name": "blue",
                "type": "uint8"
              }
            ],
            "internalType": "struct Structures.candidate[]",
            "name": "_candidates",
            "type": "tuple[]"
          },
          {
            "internalType": "bytes32",
            "name": "_nameOfElection",
            "type": "bytes32"
          },
          {
            "internalType": "uint8",
            "name": "_electionType",
            "type": "uint8"
          },
          {
            "internalType": "uint256",
            "name": "_days",
            "type": "uint256"
          },
          {
            "internalType": "address[]",
            "name": "whitelist",
            "type": "address[]"
          },
          {
            "internalType": "uint256",
            "name": "_reward",
            "type": "uint256"
          },
          {
            "internalType": "string",
            "name": "_description",
            "type": "string"
          }
        ],
        "name": "createElection",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function",
        "payable": true
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "name": "elections",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "id",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "endDate",
            "type": "uint256"
          },
          {
            "internalType": "bytes32",
            "name": "name",
            "type": "bytes32"
          },
          {
            "internalType": "address",
            "name": "owner",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "balance",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "etherReward",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "startDate",
            "type": "uint256"
          },
          {
            "internalType": "uint8",
            "name": "electionType",
            "type": "uint8"
          },
          {
            "internalType": "string",
            "name": "description",
            "type": "string"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [],
        "name": "endContract",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_electionID",
            "type": "uint256"
          }
        ],
        "name": "getElection",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          },
          {
            "internalType": "bytes32",
            "name": "",
            "type": "bytes32"
          },
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          },
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          },
          {
            "internalType": "string[]",
            "name": "",
            "type": "string[]"
          },
          {
            "internalType": "string[]",
            "name": "",
            "type": "string[]"
          },
          {
            "internalType": "uint8[]",
            "name": "",
            "type": "uint8[]"
          },
          {
            "internalType": "uint8[]",
            "name": "",
            "type": "uint8[]"
          },
          {
            "internalType": "uint8[]",
            "name": "",
            "type": "uint8[]"
          },
          {
            "internalType": "uint8",
            "name": "",
            "type": "uint8"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [
          {
            "internalType": "string",
            "name": "contains",
            "type": "string"
          }
        ],
        "name": "getElectionBasedOnName",
        "outputs": [
          {
            "components": [
              {
                "internalType": "bytes32",
                "name": "name",
                "type": "bytes32"
              },
              {
                "internalType": "address",
                "name": "owner",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "numberOfVotes",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "id",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "endDate",
                "type": "uint256"
              }
            ],
            "internalType": "struct Structures.ElectionOutline[]",
            "name": "",
            "type": "tuple[]"
          },
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_electionID",
            "type": "uint256"
          }
        ],
        "name": "getElectionDescription",
        "outputs": [
          {
            "internalType": "string",
            "name": "",
            "type": "string"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [],
        "name": "getNumberOfPolls",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_electionID",
            "type": "uint256"
          }
        ],
        "name": "getNumberOfVotes",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [],
        "name": "getOwnersElectionIDs",
        "outputs": [
          {
            "internalType": "uint256[]",
            "name": "",
            "type": "uint256[]"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_electionID",
            "type": "uint256"
          }
        ],
        "name": "getResults",
        "outputs": [
          {
            "internalType": "uint256[]",
            "name": "",
            "type": "uint256[]"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_electionID",
            "type": "uint256"
          }
        ],
        "name": "getVotes",
        "outputs": [
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "idElection",
                "type": "uint256"
              },
              {
                "internalType": "string[]",
                "name": "vote",
                "type": "string[]"
              },
              {
                "internalType": "address",
                "name": "owner",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "time",
                "type": "uint256"
              }
            ],
            "internalType": "struct Structures.Vote[]",
            "name": "",
            "type": "tuple[]"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_electionID",
            "type": "uint256"
          }
        ],
        "name": "hasResults",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_electionID",
            "type": "uint256"
          },
          {
            "internalType": "uint256[]",
            "name": "_results",
            "type": "uint256[]"
          }
        ],
        "name": "submitResults",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_electionID",
            "type": "uint256"
          },
          {
            "internalType": "bytes32",
            "name": "_hex",
            "type": "bytes32"
          }
        ],
        "name": "verifyVote",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "name": "votes",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "idElection",
            "type": "uint256"
          },
          {
            "internalType": "address",
            "name": "owner",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [],
        "name": "getOwnVotes",
        "outputs": [
          {
            "components": [
              {
                "internalType": "bytes32",
                "name": "name",
                "type": "bytes32"
              },
              {
                "internalType": "uint256",
                "name": "time",
                "type": "uint256"
              },
              {
                "internalType": "bytes32",
                "name": "hexValue",
                "type": "bytes32"
              },
              {
                "internalType": "bool",
                "name": "change",
                "type": "bool"
              },
              {
                "internalType": "uint256",
                "name": "electionID",
                "type": "uint256"
              }
            ],
            "internalType": "struct Structures.VoteTableData[]",
            "name": "",
            "type": "tuple[]"
          },
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_id",
            "type": "uint256"
          }
        ],
        "name": "donateToElection",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function",
        "payable": true
      },
      {
        "inputs": [],
        "name": "getRefund",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function",
        "payable": true
      }
    ],
    ABIAccount : [
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          }
        ],
        "name": "addressWhiteList",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [],
        "name": "endContract",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          }
        ],
        "name": "userDetails",
        "outputs": [
          {
            "internalType": "bytes32",
            "name": "password",
            "type": "bytes32"
          },
          {
            "internalType": "uint256",
            "name": "refundAllocated",
            "type": "uint256"
          },
          {
            "internalType": "uint8",
            "name": "questionNumber",
            "type": "uint8"
          },
          {
            "internalType": "bytes32",
            "name": "securityAnswer",
            "type": "bytes32"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "name": "whitelisted",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [
          {
            "internalType": "bytes32",
            "name": "hashedPassword",
            "type": "bytes32"
          }
        ],
        "name": "login",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [
          {
            "internalType": "bytes32",
            "name": "hashedAnswer",
            "type": "bytes32"
          },
          {
            "internalType": "bytes32",
            "name": "hashPassword",
            "type": "bytes32"
          }
        ],
        "name": "changePassword",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "bytes32",
            "name": "_hashedPasswordOld",
            "type": "bytes32"
          },
          {
            "internalType": "bytes32",
            "name": "_hashedPasswordNew",
            "type": "bytes32"
          }
        ],
        "name": "changePasswordWithOldPassword",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint8",
            "name": "_questionNumber",
            "type": "uint8"
          },
          {
            "internalType": "bytes32",
            "name": "_hashedAnswer",
            "type": "bytes32"
          }
        ],
        "name": "updateSecurityQuestion",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "bytes32",
            "name": "_hashedPassword",
            "type": "bytes32"
          },
          {
            "internalType": "uint8",
            "name": "_questionNumber",
            "type": "uint8"
          },
          {
            "internalType": "bytes32",
            "name": "_hashedAnswer",
            "type": "bytes32"
          }
        ],
        "name": "addNewAddress",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "sender",
            "type": "address"
          }
        ],
        "name": "isRegistered",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [],
        "name": "getSecurityQuestion",
        "outputs": [
          {
            "internalType": "string",
            "name": "",
            "type": "string"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "user",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "amount",
            "type": "uint256"
          }
        ],
        "name": "addRefund",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "checkRefundAmount",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [],
        "name": "getwhitelist",
        "outputs": [
          {
            "internalType": "uint256[]",
            "name": "",
            "type": "uint256[]"
          }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "_user",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "_electionID",
            "type": "uint256"
          }
        ],
        "name": "addUserToWhitelist",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "clear",
            "type": "address"
          }
        ],
        "name": "clearRefund",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "nonpayable",
        "type": "function"
      }
    ]
}