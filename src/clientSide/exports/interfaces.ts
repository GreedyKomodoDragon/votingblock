export interface Vote {
  name: string,
  time: number,
  hexValue: string,
  change: boolean,
  electionID: string
}

export interface VoteMin {
  vote: string,
  electionName: string
}

export interface Election {
  name: string,
  startDate: string,
  endDate: string,
  numberOfVotes: number,
  id: number
}

export interface white {
  electionName: string,
  owner: string,
  id: number
}

export interface CandidateMin {
  id: number,
  name: string,
  party: string,
  colour: string
}

export interface candidate {
  id: number,
  name: string,
  party: string,
  colour: string
}


export interface candidateWithVote {
  id: number,
  name: string,
  party: string,
  numberOfVotes: number,
  colour: string
}

export interface ElectionWithCandidates {
  id: number,
  candidates: candidate[],
  nameOfElection: string,
  endDate: number,
  type: number
}

export interface graphData {
  labels: string[],
  datasets: [{
    label: string,
    data: number[],
    backgroundColor: string[],
    borderColor: string[],
    borderWidth: number
  }]
}

export interface latest {
  name: string,
  numberOfCandidates: number,
  id: string,
  endDate: number,
  owner: string,
  description: string
}

export interface searchElection {
  id: string,
  name: string,
  numberOfVotes: number,
  endDate: number,
  owner: string,
  description: string
}