const env = process.env;

export const nodeEnv = env.NODE_ENV || 'development';

export default {
  etherAddress: "0xA2D42B89AA6a403ecfe18703a9f55a4e23CBD6f8",
  RPC_URL: 'HTTP://127.0.0.1:7545',
  mongodbUri: 'mongodb://localhost:27017/test',
  port: env.PORT || 8080,
  host: env.HOST || '0.0.0.0',
  get serverUrl() {
    return `http://${this.host}:${this.port}`;
  }
};

