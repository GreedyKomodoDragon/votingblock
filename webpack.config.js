const path = require('path');
const nodeExternals = require('webpack-node-externals');
const webpack = require('webpack');


const frontConfig = {
  target: "web",
  entry: {
    app: ["./src/clientside/index.tsx"]
  },
  resolve: {
    extensions: [".jsx", ".json", ".js", ".ts", ".tsx"]
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: {
          loader: "awesome-typescript-loader",
        }
      },
      {
        test: /\.tsx?$/,
        loader: "awesome-typescript-loader"
      },
    ]
  },
  output: {
    path: path.resolve(__dirname, "public"),
    filename: "bundle-front.js",
  },
  devServer: {
    host: '0.0.0.0', // Required for docker
    publicPath: 'public',
    contentBase: path.resolve(__dirname, "views"),
    watchContentBase: true,
    compress: true,
    port: 8080
  },
  devtool: 'inline-source-map'
}


module.exports = [frontConfig];