let stringMan = artifacts.require("./StringManipulation.sol");
let Structures = artifacts.require("./Structures.sol");
let account = artifacts.require("./accountHandling.sol");
let fundamentals = artifacts.require("./ContractFundamentals.sol");
let extended = artifacts.require("./votingExtend.sol");

module.exports = function (deployer) {
  //Libraries
  deployer.deploy(stringMan);
  deployer.deploy(Structures);

  //Base Contract
  deployer.deploy(fundamentals);

  //link parent & library
  deployer.link(Structures, account);
  deployer.link(fundamentals, account);
  deployer.link(stringMan, account);
  deployer.deploy(account).then(() =>{
    deployer.link(stringMan, extended);
    deployer.link(Structures, extended);
    deployer.link(account, extended);
    console.log(account.address)
    return deployer.deploy(extended, account.address);
  });

  


};